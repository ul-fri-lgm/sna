-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 20, 2018 at 12:50 PM
-- Server version: 5.7.23-0ubuntu0.16.04.1
-- PHP Version: 7.0.30-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `digital_atlas`
--

-- --------------------------------------------------------

--
-- Table structure for table `dialects`
--

CREATE TABLE `dialects` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_slovenian_ci NOT NULL,
  `type` tinyint(4) NOT NULL COMMENT '/1-skupina, 2-narecje,3-podnarecje',
  `id_parent` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci;

--
-- Dumping data for table `dialects`
--

INSERT INTO `dialects` (`id`, `name`, `type`, `id_parent`) VALUES
(1, 'gorenjska narečna skupina', 1, NULL),
(2, 'gorenjsko narečje', 2, 1),
(3, 'vzhodnogorenjsko podnarečje', 3, 2),
(4, 'selško narečje', 2, 1),
(5, 'dolenjska narečna skupina', 1, NULL),
(6, 'dolenjsko narečje', 2, 5),
(7, 'vzhodnodolenjsko podnarečje', 3, 6),
(8, 'severnobelokranjsko narečje', 2, 5),
(9, 'južnobelokranjsko narečje', 2, 5),
(10, 'kostelsko narečje', 2, 5),
(11, 'mešani kočevski govori', 2, 5),
(12, 'štajerska narečna skupina', 1, NULL),
(13, 'srednjesavinjsko narečje', 2, 12),
(14, 'zgornjesavinjsko narečje', 2, 12),
(15, 'solčavsko podnarečje', 3, 14),
(16, 'srednještajersko narečje', 2, 12),
(17, 'južnopohorsko narečje', 2, 12),
(18, 'kozjaško podnarečje', 3, 17),
(19, 'kozjansko-bizeljsko narečje', 2, 12),
(20, 'posavsko narečje', 2, 12),
(21, 'zagorsko-trboveljsko podnarečje', 3, 20),
(22, 'laško podnarečje ', 3, 20),
(23, 'sevniško-krško podnarečje', 3, 20),
(24, 'panonska narečna skupina', 1, NULL),
(25, 'prekmursko narečje ', 2, 24),
(26, 'slovenskogoriško narečje', 2, 24),
(27, 'prleško narečje ', 2, 24),
(28, 'haloško narečje', 2, 24),
(29, 'koroška narečna skupina', 1, NULL),
(30, 'severnopohorsko-remšniško narečje', 2, 29),
(31, 'mežiško narečje', 2, 29),
(32, 'podjunsko narečje', 2, 29),
(33, 'rožansko narečje', 2, 29),
(34, 'obirsko narečje', 2, 29),
(35, 'ziljsko narečje', 2, 29),
(36, 'kranjskogorsko podnarečje', 3, 35),
(37, 'primorska narečna skupina', 1, NULL),
(38, 'rezijansko narečje', 2, 37),
(39, 'obsoško narečje', 2, 37),
(40, 'tersko narečje', 2, 37),
(41, 'nadiško narečje', 2, 37),
(42, 'briško narečje', 2, 37),
(43, 'kraško narečje', 2, 37),
(44, 'banjško podnarečje', 3, 43),
(45, 'istrsko narečje', 2, 37),
(46, 'rižansko podnarečje', 3, 45),
(47, 'šavrinsko podnarečje', 3, 45),
(48, 'notranjsko narečje', 2, 37),
(49, 'čiško narečje', 2, 37),
(50, 'rovtarska narečna skupina', 1, NULL),
(51, 'tolminsko narečje', 2, 50),
(52, 'baško podnarečje', 3, 51),
(53, 'cerkljansko narečje', 2, 50),
(54, 'poljansko narečje', 2, 50),
(55, 'škofjeloško narečje', 2, 50),
(56, 'črnovrško narečje', 2, 50),
(57, 'horjulsko narečje', 2, 50);

-- --------------------------------------------------------

--
-- Table structure for table `lexems`
--

CREATE TABLE `lexems` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_slovenian_ci NOT NULL,
  `icon1` varchar(500) COLLATE utf8_slovenian_ci NOT NULL,
  `id_word` int(11) NOT NULL,
  `icon_color1` varchar(255) COLLATE utf8_slovenian_ci NOT NULL,
  `icon_size1` varchar(10) COLLATE utf8_slovenian_ci NOT NULL,
  `icon2` varchar(500) COLLATE utf8_slovenian_ci DEFAULT NULL,
  `icon_color2` varchar(255) COLLATE utf8_slovenian_ci DEFAULT NULL,
  `icon_size2` varchar(10) COLLATE utf8_slovenian_ci DEFAULT NULL,
  `icon3` varchar(500) COLLATE utf8_slovenian_ci DEFAULT NULL,
  `icon_color3` varchar(255) COLLATE utf8_slovenian_ci DEFAULT NULL,
  `icon_size3` varchar(10) COLLATE utf8_slovenian_ci DEFAULT NULL,
  `icon4` varchar(500) COLLATE utf8_slovenian_ci DEFAULT NULL,
  `icon_color4` varchar(255) COLLATE utf8_slovenian_ci DEFAULT NULL,
  `icon_size4` varchar(10) COLLATE utf8_slovenian_ci DEFAULT NULL,
  `star` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci;


-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE `locations` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_slovenian_ci NOT NULL DEFAULT '',
  `short_name` varchar(50) COLLATE utf8_slovenian_ci NOT NULL DEFAULT '',
  `id_subject` int(11) NOT NULL,
  `lat` decimal(10,6) NOT NULL,
  `long` decimal(10,6) NOT NULL,
  `id_dialect` int(11) NOT NULL,
  `id_subdialect` int(11) NOT NULL,
  `id_subsubdialect` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci;


-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE `subjects` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_slovenian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci;


-- --------------------------------------------------------

--
-- Table structure for table `transcriptions`
--

CREATE TABLE `transcriptions` (
  `id` int(11) NOT NULL,
  `phonetic_writing` varchar(255) COLLATE utf8_slovenian_ci NOT NULL DEFAULT '',
  `audio` varchar(255) COLLATE utf8_slovenian_ci NOT NULL DEFAULT '',
  `id_lexem` int(1) NOT NULL,
  `id_location` int(11) NOT NULL,
  `trans_text` text COLLATE utf8_slovenian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci;


-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(1) NOT NULL,
  `username` varchar(255) COLLATE utf8_slovenian_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_slovenian_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8_slovenian_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_slovenian_ci NOT NULL,
  `sess_expiration` int(60) NOT NULL DEFAULT '60'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `name`, `lastname`, `password_hash`, `sess_expiration`) VALUES
(1, 'admin', 'Admin', '', '$2y$10$bvfsnL9h3A5Dt6IaRT.m7eh/4GXmSgA/5s6wBUHJYrGgBtjDVoxYq', 60);

-- --------------------------------------------------------

--
-- Table structure for table `words`
--

CREATE TABLE `words` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_slovenian_ci NOT NULL DEFAULT '',
  `image` varchar(255) COLLATE utf8_slovenian_ci NOT NULL DEFAULT '',
  `comment` varchar(255) COLLATE utf8_slovenian_ci NOT NULL DEFAULT '',
  `id_subject` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci;


--
-- Indexes for dumped tables
--

--
-- Indexes for table `dialects`
--
ALTER TABLE `dialects`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_dialects_dialects` (`id_parent`);

--
-- Indexes for table `lexems`
--
ALTER TABLE `lexems`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_leksems_words` (`id_word`);

--
-- Indexes for table `locations`
--
ALTER TABLE `locations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_locations_dialects` (`id_dialect`),
  ADD KEY `FK_locations_dialects_2` (`id_subdialect`),
  ADD KEY `FK_locations_dialects_3` (`id_subsubdialect`),
  ADD KEY `id_subject` (`id_subject`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transcriptions`
--
ALTER TABLE `transcriptions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_location_uniq` (`id_location`,`id_lexem`),
  ADD KEY `id_word` (`id_lexem`),
  ADD KEY `id_location` (`id_location`) USING BTREE;

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `words`
--
ALTER TABLE `words`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_subject` (`id_subject`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dialects`
--
ALTER TABLE `dialects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;
--
-- AUTO_INCREMENT for table `lexems`
--
ALTER TABLE `lexems`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `locations`
--
ALTER TABLE `locations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `subjects`
--
ALTER TABLE `subjects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `transcriptions`
--
ALTER TABLE `transcriptions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `words`
--
ALTER TABLE `words`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `dialects`
--
ALTER TABLE `dialects`
  ADD CONSTRAINT `FK_dialects_dialects` FOREIGN KEY (`id_parent`) REFERENCES `dialects` (`id`);

--
-- Constraints for table `lexems`
--
ALTER TABLE `lexems`
  ADD CONSTRAINT `FK_leksems_words` FOREIGN KEY (`id_word`) REFERENCES `words` (`id`);

--
-- Constraints for table `locations`
--
ALTER TABLE `locations`
  ADD CONSTRAINT `FK_locations_dialects` FOREIGN KEY (`id_dialect`) REFERENCES `dialects` (`id`),
  ADD CONSTRAINT `FK_locations_dialects_2` FOREIGN KEY (`id_subdialect`) REFERENCES `dialects` (`id`),
  ADD CONSTRAINT `FK_locations_dialects_3` FOREIGN KEY (`id_subsubdialect`) REFERENCES `dialects` (`id`),
  ADD CONSTRAINT `locations_ibfk_1` FOREIGN KEY (`id_subject`) REFERENCES `subjects` (`id`);

--
-- Constraints for table `transcriptions`
--
ALTER TABLE `transcriptions`
  ADD CONSTRAINT `transcriptions_ibfk_1` FOREIGN KEY (`id_location`) REFERENCES `locations` (`id`),
  ADD CONSTRAINT `transcriptions_ibfk_2` FOREIGN KEY (`id_lexem`) REFERENCES `lexems` (`id`);

--
-- Constraints for table `words`
--
ALTER TABLE `words`
  ADD CONSTRAINT `FK_words_subjects` FOREIGN KEY (`id_subject`) REFERENCES `subjects` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
