<?php

/*
Slovenski narečni atlas / Slovenian dialectal atlas
    Copyright (C) 2017  Gregor Šajn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

Class Admin_model extends CI_Model {

	function __construct() 
	{
		parent::__construct();

		$this->load->database();
    }

	function users()
	{
		$this->db->select('t1.*');
		$this->db->from("users AS t1");
		$result=$this->db->get()->result_array();

		return $result;
	}

	function user($id_user)
	{
		$this->db->select('t1.*');
		$this->db->from("users AS t1");
		$this->db->where('t1.id',$id_user);
		$user=$this->db->get()->row_array();

		return $user;
	}

	function perform_login($username,$password)
	{
		$this->db->select("
			t1.*,
		");
		$this->db->from('users AS t1');
		$this->db->where('t1.username',$username);
		$user=$this->db->get()->row_array();

		if(password_verify($password, $user['password_hash'])) 
		{
			return $user;
		}
		else
		{
			return false;
		}
	
	}

	function subject($id)
	{
		$this->db->select('*');
		$this->db->from('subjects AS t1');
		$this->db->where('id',$id);
		$subject=$this->db->get()->row_array();

		return $subject;
	}

    function subjects()
    {
        $this->db->select('*');
        $this->db->from('subjects AS t1');
        $subjects=$this->db->get()->result_array();

        $data=array();
        foreach($subjects as $subject)
        {
            $data[$subject['id']]=$subject['title'];
        }

        return $data;
    }

	function word($id)
    {
    	$this->db->select('t1.*');
    	$this->db->from("words AS t1");
    	$this->db->where("t1.id",$id);
    	$word=$this->db->get()->row_array();

    	if(isset($word['image']))
    	{
    		$word['image']=$word['image'];
    	}

    	
    	if(isset($word['comment']))
    	{
    		$word['comment']=$word['comment'];
    	}

    	
    	return $word;
    }

    function transcriptions($id_word=0,$id_location=0,$id_subject=0,$id_transcription=0,$id_lexem=0)
    {
    	$this->db->select('
            t1.*,
            t2.name,
            t3.title AS lexem_title,
            t3.star AS lexem_star,
            t4.title AS word_title,
            t5.title AS subject_title,
        ');
    	$this->db->from('transcriptions AS t1');
        $this->db->join('locations AS t2','on t1.id_location=t2.id','LEFT');
        $this->db->join('lexems AS t3','on t1.id_lexem=t3.id','LEFT');
        $this->db->join('words AS t4','on t3.id_word=t4.id','LEFT');
        $this->db->join('subjects AS t5','on t4.id_subject=t5.id','LEFT');
        if($id_subject)
        {
            $this->db->where('t4.id_subject',$id_subject);
        }
        if($id_word)
        {
            $this->db->where('t3.id_word',$id_word);
        }
        if($id_location)
        {
            $this->db->where('t1.id_location',$id_location);
        }

        if($id_transcription)
        {
            $this->db->where('t1.id!=',$id_transcription);
        }

        if($id_lexem)
        {
            $this->db->where('t1.id_lexem',$id_lexem);
        }
    	
    	$this->db->order_by('lexem_title','ASC');

    	$result=$this->db->get()->result_array();

    	return $result;
    }

    function transcription($id)
    {
        $this->db->select('
            t1.*,
            t2.title AS lexem_title,
            t3.id_subject,
            t3.id AS id_word,
        ');
        $this->db->from('transcriptions AS t1');
        $this->db->join('lexems AS t2','on t1.id_lexem=t2.id');
        $this->db->join('words AS t3','on t2.id_word=t3.id');
        $this->db->where('t1.id',$id);

        $result=$this->db->get()->row_array();

        return $result;
    }

    function titles($id_word)
    {
        $this->db->select('id,title');
        $this->db->from('lexems');
        $this->db->where('id_word',$id_word);
        $this->db->group_by('title,id');
        $this->db->order_by('title','ASC');
        $titles=$this->db->get()->result_array();

        $data=array();

        foreach($titles as $title)
        {
            $data[$title['id']]=$title['title'];
        }

        return $data;

    }

    function locations($id_subject)
    {
    	$this->db->select('t2.*');
    	$this->db->from('subjects AS t1');
        $this->db->join('locations AS t2','t1.id=t2.id_subject');
        if($id_subject)
        {
            $this->db->where('t2.id_subject',$id_subject);
        }
        
    	$this->db->order_by('t2.name','ASC');
        $this->db->group_by('t2.id');

    	$result=$this->db->get()->result_array();

        $data=array();

        foreach($result as $location)
        {
             $data[$location['id']]=$location['name'];
        }
        
        return $data;
    }

    function locations_all()
    {
        $this->db->select('t1.*, t2.title');
        $this->db->from('locations AS t1');
        $this->db->join('subjects AS t2', 't1.id_subject=t2.id');
        $this->db->order_by('name','ASC');
        $result=$this->db->get()->result_array();

        return $result;
    }

    function location($id_location)
    {
    	$this->db->select('*');
    	$this->db->from('locations');
    	$this->db->where('id',$id_location);

    	$location=$this->db->get()->row_array();

    	return $location;
    }

    function subject_locations($id_location)
    {
        $this->db->select('*');
        $this->db->from('subject_locations');
        $this->db->where('id_location',$id_location);
        $subject_locations=$this->db->get()->result_array();

        $data=array();
        foreach($subject_locations as $entry)
        {
            $data[$entry['id_subject']]=$entry['id_subject'];
        }

        return $data;
    }

    function dialects($options=false)
    {
        $this->db->select('*');
        $this->db->from('dialects');
        $this->db->where('type',1);
        $dialects=$this->db->get()->result_array();

        if($options)
        {
            $data=array();
            foreach($dialects as $dialect)
            {
                $data[$dialect['id']]=$dialect['name'];
            }

            return $data;
        }

        return $dialects;
    }

    function subdialects($id_dialect,$options=false)
    {
        $this->db->select('*');
        $this->db->from('dialects');
        $this->db->where('type',2);
        $this->db->where('id_parent',$id_dialect);
        $subdialects=$this->db->get()->result_array();

        if($options)
        {
            $data=array();
            foreach($subdialects as $dialect)
            {
                $data[$dialect['id']]=$dialect['name'];
            }

            return $data;
        }

        return $subdialects;
    }

    function subsubdialects($id_subdialect,$options=false)
    {
        $this->db->select('*');
        $this->db->from('dialects');
        $this->db->where('type',3);
        $this->db->where('id_parent',$id_subdialect);
        $subsubdialects=$this->db->get()->result_array();

        if($options)
        {
            $data=array();
            foreach($subsubdialects as $dialect)
            {
                $data[$dialect['id']]=$dialect['name'];
            }

            return $data;
        }

        return $subsubdialects;
    }

    function locations_by_word($id_word)
    {
        //get selected locations
        $this->db->select("
                t2.*
            ");
        $this->db->from('transcriptions as t1');
        $this->db->join('locations as t2','on t1.id_location=t2.id');
        $this->db->where('t1.id_word',$id_word);
        $selected_locations=$this->db->get()->result_array();

        //all locations
        $this->db->select('*');
        $this->db->from('locations');
        $locations=$this->db->get()->result_array();

        //init data array for unselected locations
        $data=array();

        foreach($locations as $location)
        {
            if(!in_array($location,$selected_locations))
            {
                $data[$location['id']]=$location['name'];
            }
        }

        return $data;
    }

    function lexems($id_word=0,$id_subject=0,$options=false)
    {
        $this->db->select('
            t1.*,
            t2.title AS word_title,
            t3.title AS subject_title,
        ');
        $this->db->from('lexems AS t1');
        $this->db->join('words AS t2','t1.id_word=t2.id');
        $this->db->join('subjects AS t3','t2.id_subject=t3.id');
        if($id_subject)
        {
            $this->db->where('t2.id_subject',$id_subject);
        }
        if($id_word)
        {
            $this->db->where('t1.id_word',$id_word);
        }
        $this->db->order_by('t1.title','ASC');
        $lexems=$this->db->get()->result_array();

        if($options)
        {
            $data=array();
            foreach($lexems as $lexem)
            {
                $data[$lexem['id']]=$lexem['title'];
            }

            return $data;
        }

        return $lexems;
    }

    function word_lexems($id_word,$options=false)
    {
        $this->db->select('
            t1.*,
            t2.title AS word_title,
        ');
        $this->db->from('lexems AS t1');
        $this->db->join('words AS t2','t1.id_word=t2.id');
        $this->db->where('t1.id_word',$id_word);
        $this->db->order_by('t1.title','ASC');
        $lexems=$this->db->get()->result_array();

        if($options)
        {
            $data=array();
            foreach($lexems as $lexem)
            {
                $data[$lexem['id']]=$lexem['title'];
            }

            return $data;
        }

        return $lexems;
    }

    function lexem($id)
    {
        $this->db->select('t1.*,t2.id_subject');
        $this->db->from('lexems AS t1');
        $this->db->join('words AS t2','t2.id=t1.id_word');
        $this->db->where('t1.id',$id);
        $lexem=$this->db->get()->row_array();

        return $lexem;
    }


    function subject_title_exists($title,$id=0)
    {
        $this->db->from('subjects');
        $this->db->where('title',$title);
        if($id)
        {
            $this->db->where('id!=',$id);
        }
        
        $exists=$this->db->count_all_results();

        return $exists;
    }

    function words_title_exists($title,$id=0,$id_subject)
    {
        $this->db->from('words');
        $this->db->where('title',$title);
        if($id)
        {
            $this->db->where('id!=',$id);
        }
        $this->db->where('id_subject',$id_subject);
        $exists=$this->db->count_all_results();

        return $exists;
    }

    function location_exists($field,$parameter, $id_subject, $id=0)
    {
        $this->db->select('name');
    	$this->db->from('locations');
    	$this->db->where($field,$parameter);
        $this->db->where('id_subject', $id_subject);
        if($id)
        {
            $this->db->where('id!=',$id);
        }
        
    	$exists=$this->db->count_all_results();

    	return $exists;
    }

    function location_short_name_exists($short_name, $id_subject, $id=0)
    {
        $this->db->select('short_name');
        $this->db->from('locations');
        $this->db->where('id_subject',$id_subject);
        $this->db->where('short_name',$short_name);
        if($id)
        {
            $this->db->where('id!=',$id);
        }
        
        $names=$this->db->get()->result_array();

        $identical=false;
        foreach($names as $name)
        {
            if(strcmp($name['short_name'],$short_name)==0)
            {
                $identical=true;
            }
        }

        return $identical;
    }

    function coordinates_exists($lat,$long,$id=0, $id_subject)
    {
    	$this->db->from('locations');
    	$this->db->where('lat',$lat);
    	$this->db->where('long',$long);
        $this->db->where('id_subject',$id_subject);
        if($id)
        {
            $this->db->where('id!=',$id);
        }
        
    	$exists=$this->db->count_all_results();

    	return $exists;
    }

    function lexem_title_exists($id=0,$title,$id_subject,$id_word)
    {

    }

    function username_exists($username,$id_user)
    {
        $this->db->from('users');
        $this->db->where('username',$username);
        $this->db->where('id!=',$id_user);
        $exists=$this->db->count_all_results();

        return $exists;
    }

    function subject_links($id_subject)
    {
    	//words
    	$this->db->from('words');
    	$this->db->where('id_subject',$id_subject);
    	$word_c=$this->db->count_all_results();

    	return $word_c;
    }

    function locations_links($id_location)
    {
    	$this->db->select('*');
    	$this->db->from('transcriptions');
    	$this->db->where('id_location',$id_location);
    	$links=$this->db->get()->result_array();

    	return $links;
    }

     function word_links($id_word)
    {
        $this->db->select('*');
        $this->db->from('lexems');
        $this->db->where('id_word',$id_word);
        $links=$this->db->get()->result_array();

        return $links;
    }

    function lexem_links($id)
    {
        $this->db->select('*');
        $this->db->from('transcriptions');
        $this->db->where('id_lexem',$id);
        $links=$this->db->get()->result_array();

        return $links;
    }
}