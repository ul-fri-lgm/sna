<?php

/*
Slovenski narečni atlas / Slovenian dialectal atlas
    Copyright (C) 2017  Gregor Šajn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
    
Class Home_model extends CI_Model {

	function __construct() 
	{
		parent::__construct();
		
		$this->load->database();
    }

    function subjects($options=false)
    {
    	$this->db->select('t1.*');
    	$this->db->from("subjects AS t1");
        $this->db->order_by('title','ASC');
		$subjects=$this->db->get()->result_array();

		if($options=='select')
		{
            //init 
            $data=array();

            foreach($subjects as $subject)
            {
                $data[$subject['id']]=$subject['title'];
            }

            return $data;
		}
        else
        {
            return $subjects;
        }
		
    }

    function subject_words($id_subject,$options='select')
    {
    	$this->db->select('t2.*');
    	$this->db->from("subjects AS t1");
    	$this->db->join("words as t2","t2.id_subject=t1.id");
    	$this->db->where("t1.id",$id_subject);
        $this->db->order_by('t2.title','ASC');
    	$words=$this->db->get()->result_array();

    	if($options=='select')
    	{
    		//init 
    		$data=array();

    		foreach($words as $word)
    		{
    			$data[$word['id']]=$word['title'];
    		}

    		return $data;
    	}

		return $words;

    }

    function words_trascriptions($id_word,$id_subject)
    {
        if($id_word)
        {
            //init
            $used=array();
            $result=array();

            $this->db->select('
                t2.*,
                t3.name AS dialect_title,
                t4.name AS subdialect_title,
                t5.name AS subsubdialect_title,
            ');
            $this->db->from('subjects AS t1');
            $this->db->join('locations AS t2','t1.id=t2.id_subject');
            $this->db->join('dialects AS t3','t2.id_dialect=t3.id');
            $this->db->join('dialects AS t4','t2.id_subdialect=t4.id');
            $this->db->join('dialects AS t5','t2.id_subsubdialect=t5.id','LEFT');
            $this->db->where('t2.id_subject',$id_subject);
            $locations=$this->db->get()->result_array();



            foreach($locations as $i=>$location)
            {   
                $pixels=$this->convert_to_pixels($location['lat'],$location['long']);

                $result[$i]['x']=$pixels['x'];
                $result[$i]['y']=$pixels['y'];
                $result[$i]['name']=$location['name'];
                $result[$i]['short_name']=$location['short_name'];
                $result[$i]['dialect']=$location['dialect_title'];
                $result[$i]['subdialect']=$location['subdialect_title'];
                if(isset($location['subsubdialect_title']))
                {
                    $result[$i]['subsubdialect']=$location['subsubdialect_title'];
                }
                

                $this->db->select('
                    t1.*,
                    t2.*,
                ');
                $this->db->from('transcriptions AS t1');
                $this->db->join('lexems AS t2','t1.id_lexem=t2.id');
                $this->db->where('t2.id_word',$id_word);
                $this->db->where('t1.id_location',$location['id']);
                $transcription=$this->db->get()->row_array();

                //return array('odgovor'=>$locations);

                if($transcription)
                {
                     $result[$i]['pin1']=$transcription['icon1'];
                     $result[$i]['pin2']=$transcription['icon2'];
                     $result[$i]['pin3']=$transcription['icon3'];
                     $result[$i]['pin4']=$transcription['icon4'];
                     $result[$i]['id']=$transcription['id'];
                     $result[$i]['title']=$transcription['title'];
                     $result[$i]['phonetic_writing']=$transcription['phonetic_writing'];
                     $result[$i]['audio']=$transcription['audio'];
                     $result[$i]['id_word']=$transcription['id_word'];
                     $result[$i]['id_location']=$transcription['id_location'];
                     $result[$i]['trans_text']=$transcription['trans_text'];
                     $result[$i]['icon1']=$transcription['icon1'];
                     $result[$i]['icon_color1']=$transcription['icon_color1'];
                    $transcription['icon_size1'] == 'small' ? $result[$i]['icon_size1']= '12' : $result[$i]['icon_size1']= '18';

                    $result[$i]['icon2']=$transcription['icon2'];
                    $result[$i]['icon_color2']=$transcription['icon_color2'];
                    $transcription['icon_size2'] == 'small' ? $result[$i]['icon_size2']= '12' : $result[$i]['icon_size2']= '18';
                    $result[$i]['icon3']=$transcription['icon3'];
                    $result[$i]['icon_color3']=$transcription['icon_color3'];
                    $transcription['icon_size3'] == 'small' ? $result[$i]['icon_size3']= '12' : $result[$i]['icon_size3']= '18';
                    $result[$i]['icon4']=$transcription['icon4'];
                    $result[$i]['icon_color4']=$transcription['icon_color4'];
                    $transcription['icon_size4'] == 'small' ? $result[$i]['icon_size4']= '12' : $result[$i]['icon_size4']= '18';
                    $result[$i]['star']=$transcription['star'];

                    //return array('odgovor'=>$result);
                     
                     //only allowed chars
                     $code_title = preg_replace('/[^a-zA-Z0-9]/','', $transcription['title']);
                     $result[$i]['code_title']=$code_title;

                    if(!array_key_exists($transcription['title'],$used))
                    {
                        if($transcription['icon1'])
                        {
                            $used[$transcription['title']]=array(
                                'icon1'=>$transcription['icon1'],
                                'icon_color1'=>$transcription['icon_color1'],
                                'icon_size1'=>$transcription['icon_size1'],
                                'icon2'=>$transcription['icon2'],
                                'icon_color2'=>$transcription['icon_color2'],
                                'icon_size2'=>$transcription['icon_size2'],
                                'icon3'=>$transcription['icon3'],
                                'icon_color3'=>$transcription['icon_color3'],
                                'icon_size3'=>$transcription['icon_size3'],
                                'icon4'=>$transcription['icon4'],
                                'icon_color4'=>$transcription['icon_color4'],
                                'icon_size4'=>$transcription['icon_size4'],
                                'star'=>$transcription['star'],
                                'code_title'=>$code_title,
                            );
                        }
                    }
                }
                else
                {
                    //return array('odgovor'=>'no transcription');
                    $result[$i]['pin1']="no_title";
                    $result[$i]['title']='no_title';
                    $result[$i]['code_title']='no_title';
                }
            }
            //add no title for the last
            $used['no_title']=array(
                'icon1'=>'/',
                'icon_color1'=>"#000000",
                'icon_size1'=>"normal",
                'icon2'=>'',
                'icon_color2'=>"#000000",
                'icon_size2'=>"normal",
                'icon3'=>'',
                'icon_color3'=>"#000000",
                'icon_size3'=>"normal",
                'icon4'=>'',
                'icon_color4'=>"#000000",
                'icon_size4'=>"normal",
                'code_title'=>'no_title'
            );

            $data=array(
                'legend'=>$used,
                'transcriptions'=>$result
            );

            return $data;

        }
        else
        {
            return;
        }
    	
    }

    function convert_to_pixels($lat,$long)
    {
        //init
        //corners min, max in geo
        $min_lat=46.980904;
        $min_long=13.113429;
        $max_lat=45.389575;
        $max_long=16.674917;

        //height,width of canvas
        $height=720;
        $width=1100;

        //error
        $height_err=0;
        $width_err=0;
		
        //calculate pixes per degree
        $ppd_height=number_format($height/($min_lat-$max_lat),6);
        $ppd_width=number_format($width/abs($min_long-$max_long),6);

        //perform calculations
        $delta_x=number_format(abs($min_long-$long),6);
        $delta_y=number_format(abs($min_lat-$lat),6);

        $x=(int)($delta_x*$ppd_width)-$width_err;
        $y=(int)($delta_y*$ppd_height)+$height_err;

        $result=array(
            'x'=>$x,
            'y'=>$y,
        );

        return $result;
    }

    function transcriptions_count($id_word)
    {
        $this->db->from('transcriptions AS t1');
        $this->db->join('lexems AS t2','t1.id_lexem=t2.id');
        $this->db->where('t2.id_word',$id_word);
        $c=$this->db->count_all_results();

        return $c;

    }

    function word_title($id_word)
    {
    	$this->db->select('t1.title');
    	$this->db->from("words AS t1");
    	$this->db->where("t1.id",$id_word);
    	$word=$this->db->get()->row_array();

    	return $word;
    }

    function word($id)
    {
    	$this->db->select('t1.*');
    	$this->db->from("words AS t1");
    	$this->db->where("t1.id",$id);
    	$word=$this->db->get()->row_array();

    	if(isset($word['image']) and $word['image'])
    	{
    		$word['image']=$word['image'];
    	}
    	else
    	{
    		$word['image']='';
    	}
    	
    	if(isset($word['comment']) and $word['comment'])
    	{
    		$word['comment']=base_url().$word['comment'].'#zoom=100';
    	}
    	else
    	{
    		$word['comment']='';
    	}
    	
    	return $word;
    }

    private function get_icons()
    {
    	//init
    	$icons=array();

    	$images = glob('images/icons/*.{jpeg,gif,png,svg}', GLOB_BRACE);

    	foreach($images as $image)
    	{
    		$icons[]=basename($image);
    	}

		return $icons;
    }
}