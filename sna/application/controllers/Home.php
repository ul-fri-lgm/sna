<?php
/*
Slovenski narečni atlas / Slovenian dialectal atlas
    Copyright (C) 2017  Gregor Šajn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct() 
	{
		parent::__construct();

		$this->url=base_url().'home/';

		$this->load->model('home_model');
    }
	
	public function index()
	{
		$data=array(
			'title'=>'Slovenski narečni atlas',
		);

		$data['subjects']=array(0=>'Izberite temo')+$this->home_model->subjects('select');
		$data['words']=array(0=>'Izberite besedo');

		$this->load_master_view('home/index_view.php',$data);
	}

	function ajax_change_subject()
	{
		$id_subject=$this->input->post('id_subject',true);

		//load words
		$data=array();

		$data['words']=array(0=>'Izberite besedo')+$this->home_model->subject_words($id_subject,'select');

		$data['status']='ok';

		echo json_encode($data);

	}

	function ajax_add_locations()
	{
		$id_word=$this->input->post('id_word',true);
		$id_subject=$this->input->post('id_subject',true);

		//get transprictions and locations
		$result=$this->home_model->words_trascriptions($id_word,$id_subject);

		$data['words_transcriptions']=$result['transcriptions'];
        //echo json_encode($result['odgovor']);
        //return;
		$data['word']=$this->home_model->word($id_word);

		$data['legend']=$result['legend'];
		$data['transcriptions_count']=$this->home_model->transcriptions_count($id_word);
		
		//load views
		$data['view_legend']=$this->load->view('home/legend_view.php',$data,true);
		$data['view_modals']=$data['view']=$this->load->view('home/transcription_popup_view.php',$data,true);

		$data['status']='ok';

		echo json_encode($data);

	}

	function ajax_update_word_data()
	{
		$id_word=$this->input->post('id_word',true);

		


		$data['status']='ok';

		echo json_encode($data);
	}

	function about()
	{
		$data=array(
			'title'=>'O spletni aplikaciji',
		);

		$this->load_master_view('home/about.php',$data);
	}

	function load_master_view($page,$data)
	{
		$this->load->view('header.php',$data);
		$this->load->view($page);
		$this->load->view('footer.php',$data);
	}
}
