<?php

/*
Slovenski narečni atlas / Slovenian dialectal atlas
    Copyright (C) 2017  Gregor Šajn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

?>

<table class="table table-hover table-striped" id="search_locations-table">
  <thead>
    <tr id="locations_header">
      <th width="10%">Št.</th>
      <th width="20%">Leksem</th>
      <th width="40%">Beseda (tema)</th>
      <th width="15%" class="text-center">Simbol leksema</th>
      <th width="10%" class="text-center">Odstrani</th>
    </tr>
  </thead>
  <tbody>
  <?
  foreach($lexems as $i=>$lexem)
  {
    ?>
    <tr id="row-<?=$lexem['id']?>" onclick="window.document.location='<?=$this->url?>edit_lexem/<?=$lexem['id']?>'">
      <td><?=$i+1?>.</td>
      <td><?=$lexem['star'] === '1' ? $lexem['title'] . '*' : $lexem['title']?></td>
      <td><?=$lexem['word_title']?> (<?=$lexem['subject_title']?>)</td>
      <td class="SIMBola text-center"><span style="color:<?=$lexem['icon_color1']?>; font-size: <?= $lexem['icon_size1'] && $lexem['icon_size1'] === 'normal' ? '17px' : '13px'?>"><?=$lexem['icon1']?></span><span style="color:<?=$lexem['icon_color2']?>; font-size: <?= $lexem['icon_size2'] && $lexem['icon_size2'] === 'normal' ? '17px' : '13px'?>"><?=$lexem['icon2'] && $lexem['icon2']!== 0 ? $lexem['icon2'] : ''?></span><span style="color:<?=$lexem['icon_color3']?>; font-size: <?= $lexem['icon_size3'] && $lexem['icon_size3'] === 'normal' ? '17px' : '13px'?>"><?=$lexem['icon3'] && $lexem['icon3']!== 0 ? $lexem['icon3'] : ''?></span><span style="color:<?=$lexem['icon_color4']?>; font-size: <?= $lexem['icon_size4'] && $lexem['icon_size4'] === 'normal' ? '17px' : '13px'?>"><?=$lexem['icon4'] && $lexem['icon4']!== 0 ? $lexem['icon4'] : ''?></span></td>
      <td class="text-center"><span class="glyphicon glyphicon-hover glyphicon-remove text-danger" onclick="if(confirm('Izbriši leksem <?php echo $lexem['title'] ?>?')){ajax_lexem_delete(<?=$lexem['id']?>)};event.stopPropagation();"></span></td>
    </tr>
    <?
  }

  ?>
  </tbody>
</table>