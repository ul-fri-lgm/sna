<?php

/*
Slovenski narečni atlas / Slovenian dialectal atlas
    Copyright (C) 2017  Gregor Šajn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

?>

<div class="container container-panel">
  <?
  $this->load->view('admin/index_view.php');
  ?>
  <div class="row" style="overflow: none">
    <?
    $this->load->view('admin/admin_nav.php');
    ?>
    <!-- tukaj se naloži poljuben pogled -->
    <div class="col-sm-9">
      <ol class="breadcrumb">
          <li><a href="<?=$this->url?>locations">Lokacije</a></li>
      </ol>
      <div class="row">
        <div class="col-sm-10"><h4 class="text-muted">Lokacije</h4></div>
        <div class="col-sm-2 text-right"><a class="btn btn-success" href="<?=$this->url?>add_location" title="Dodaj lokacijo">Dodaj</a></div>
      </div>
      <!-- alerts -->
      <?
      if(isset($status) and $status=='ok')
      {
        ?>
        <div class="alert alert-success fade in" id="alert_ok" role="alert">
        <button type="button" class="close">
            <span aria-hidden="true">&times;</span>
          </button>
        <span class="text-success glyphicon glyphicon-ok"></span> Spremembe uspešno shranjene!
        </div>
        <?
      }
      ?>
      <div class="alert fade in hidden" id="alert" role="alert">
      <button type="button" class="close" onclick="hide_alert()">
        <span aria-hidden="true">&times;</span>
      </button>
        <span id="alert_text"></span>
      </div>
      <br>

      <div class="form-inline">
        <input type="text" id="search_locations" class="form-control" onkeyup="search_locations()" value="" placeholder="Iskanje lokacij..." style="width:300px;"/> <span id="search-loader" class="hidden"><span class="glyphicon glyphicon-cog spin"></span></span>
      </div>
      <br>


      <div id="locations" class="list">
      <table class="table table-hover table-striped" id="search_locations-table">
        <thead>
          <tr id="locations_header">
            <th width="7%">Št.</th>
            <th width="18%">Kraj</th>
            <th width="10%">Kratica</th>
            <th width="9%">Tema</th>
            <th width="23%" class="text-center">Zemljepisna širina (lat)</th>
            <th width="23%" class="text-center">Zemljepisna dolžina (long)</th>
            <th class="text-center">Odstrani</th>
          </tr>
        </thead>
        <tbody>
        <?
        foreach($locations as $i=>$location)
        {
          ?>
          <tr id="row-<?=$location['id']?>" onclick="window.document.location='<?=$this->url?>edit_location/<?=$location['id']?>'">
            <td><?=$i+1?>.</td>
            <td><?=$location['name']?></td>
            <td><?=$location['short_name']?></td>
            <td><?=$location['title']?></td>
            <td class="text-center"><?=number_format($location['lat'],2)?></td>
            <td class="text-center"><?=number_format($location['long'],2)?></td>
            <td class="text-center"><span class="glyphicon glyphicon-hover glyphicon-remove text-danger" onclick="if(confirm('Izbriši lokacijo <?php echo $location['name'] ?>?')){ajax_location_delete(<?=$location['id']?>)};event.stopPropagation();"></span></td>
          </tr>
          <?
        }

        ?>
        </tbody>
      </table>
      </div>

    </div>
  </div>
</div>


<script type="text/javascript">
$('.close').click(function() {
    $('#alert_ok').fadeOut(500);
});

$(document).ready(function() {
    $.extend($.expr[":"], {
      "containsIN": function(elem, i, match, array) {
      return (elem.textContent || elem.innerText || "").toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
    }
  });
    
});



function ajax_location_delete(id_location)
{

  $.ajax({
        type: "POST",
        url: "<?=$this->url?>ajax_location_delete",
        cache: false,               
        data:{
          id_location:id_location
        },
        dataType: "json",
        success: function(data){  
            if(data.status=='ok')
            { 
                //remove row
                $('#row-'+id_location).remove();
            }
            else
            {
              var errors='';
              $.each(data.errfields, function(i, error){
                  errors+=error+'\n';
              });
              status('error',errors);
            }      
        },
        error: function(){                      
            alert('Prišlo je do napake pri nalaganju podatkov. Prosimo poskusite ponovno!');
        }
  });
}


function status(status,text)
{
  $('#alert').fadeIn(400);
  //change div color
  if(status=='ok')
  {
    $('#alert').removeClass();
    $('#alert').addClass('alert alert-success');
    $('#alert_text').html("<span class='glyphicon glyphicon-ok text-success'></span> "+text);
  }
  else
  {
    $('#alert').removeClass();
    $('#alert').addClass('alert alert-danger');
    $('#alert_text').html("<span class='glyphicon glyphicon-exclamation-sign text-danger'></span> "+text);
  }
}

function hide_alert()
{
  $('#alert').fadeOut(500);
  $('alert').addClass('hidden'); 
}


function search_locations()
{ 
  


    $('#search-loader').show();
    var src=$('#search_locations').val();

    if(src)
    {
      $('#search_locations-table tr').hide();
      $('#locations_header').show();
      var src_words=src.split(" ");

      var items = [];
      var search='';

      $.each(src_words, function (index, word) {

        word=word.toLowerCase();
        search+="containsIN('"+word+"'):";
      });

      search = search.slice(0, -1);

      //console.log(search);

      var items=$('#search_locations-table tr:'+search);

      items.show();
    }
    else
    {
      $('#search_locations-table tr').show();
    }

    $('#search-loader').hide();
  
}


</script>