<?php

/*
Slovenski narečni atlas / Slovenian dialectal atlas
    Copyright (C) 2017  Gregor Šajn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

?>

<div class="container container-panel">
  <?
  $this->load->view('admin/index_view.php');
  ?>
  <div class="row" style="overflow: none">
    <?
    $this->load->view('admin/admin_nav.php');
    ?>
    <!-- tukaj se naloži poljuben pogled -->
    <div class="col-sm-9">
      <ol class="breadcrumb">
          <li><a href="<?=$this->url?>transcriptions">Fonetični zapisi</a></li>
      </ol>
      <div class="row">
        <div class="col-sm-10"><h4 class="text-muted">Fonetični zapisi</h4></div>
        <div class="col-sm-2 text-right"><a class="btn btn-success" id="add_transcription" href="<?=$this->url?>add_transcription/" title="Dodaj transkripcijo">Dodaj</a></div>
      </div>
      <?
      
      if(isset($status) and $status=='ok')
      {
          ?>
          <div class="alert alert-success fade in" id="alert_ok" role="alert">
          <button type="button" class="close">
            <span aria-hidden="true">&times;</span>
          </button>
          <span class="glyphicon glyphicon-ok text-success"></span> Spremembe uspešno shranjene!
          </div>
          <?
      }
      ?>
      <div class="alert fade in hidden" id="alert" role="alert">
      <button type="button" class="close">
        <span aria-hidden="true">&times;</span>
      </button>
        <span id="alert_text"></span>
      </div>
      <br>
      <div class="form-inline">
        <div class="form-group">
            <label for="subjects" class="control-label">Tema</label>

              <select class="form-control input-sm" id="subjects" style="width:200px;" onchange="ajax_change_subject();">
              <? 
              foreach($subjects as $id=>$subject)
              {
                ?><option value="<?=$id?>" <?=$id==$id_subject?'selected="selected"':''?>><?=$subject?></option><?
              }
              ?>
              </select>
         <br>   
        </div>
        <div class="form-group">
            <label for="words" class="ccontrol-label">Beseda</label>

              <select class="form-control input-sm" id="words" style="width:200px;" onchange="ajax_load_transcriptions();">
              <? 
              foreach($words as $id=>$word)
              {
                ?><option value="<?=$id?>" <?=$id==$id_word?'selected="selected"':''?>><?=$word?></option><?
              }
              ?>
              </select>
            <br>
        </div>
        <div class="form-group">
            <label for="titles" class="control-label">Leksem</label>
              <select class="form-control input-sm" id="titles" style="width:200px;" onchange="ajax_update_transcriptions_titles();">
              <? 
              foreach($lexems as $id=>$title)
              {
                ?><option value="<?=$id?>" <?=$id==$id_lexem?'selected="selected"':''?>><?=$title?></option><?
              }
              ?>
              </select>  
        </div>
        <br>
        <br>
        <div class="form-group">
            <label for="locations" class="control-label">Kraj</label>
              <select class="form-control input-sm" id="locations-select" style="width:200px;" onchange="ajax_change_location();">
              <? 
              foreach($locations as $id=>$location)
              {
                ?><option value="<?=$id?>" <?=$id==$id_location?'selected="selected"':''?>><?=$location?></option><?
              }
              ?>
              </select>  
        </div>
      </div>
      <br>
      <div id="transcriptions_table" class="list">
      <?
      if(isset($view))
      {
        echo $view;
      }
      ?>
      </div>

    </div>
  </div>
</div>


<script type="text/javascript">

$('.close').click(function() {
    $('#alert_err').fadeOut(500);
    $('#alert_ok').fadeOut(500);
});

function ajax_change_subject()
{
  var id_subject=$("#subjects option:selected").val();
  var id_location=$("#locations-select option:selected").val();

  $.ajax({
        type: "POST",
        url: "<?=$this->url?>ajax_change_subject",
        cache: false,               
        data: {
          id_subject: id_subject,
          id_location: id_location,
        },
        dataType: "json",
        success: function(data){ 
          if(data.status=='ok')
          {
            var options;
            $('#words').empty();

            $.each(data.words, function(id, title){
                options += '<option value=' + id + '>' + title + '</option>';
            });
            
            $('#words').append(options);

            //update locations
            var locations;
            $('#locations-select').empty();

            //sort by name
            var sortable = [];
            for (var location in data.locations) {
                sortable.push([location, data.locations[location]]);
            }
            var first=sortable.shift();
            sortable.sort(function(a, b) {
                return a[1].localeCompare(b[1]);
            });
            sortable.unshift(first);

            $.each(sortable, function(id, title){
                locations += '<option value=' + title[0] + '>' + title[1] + '</option>';
            });
            
            $('#locations-select').append(locations);


            //update table
            $('#transcriptions_table').html(data.view);

          }    
        },
        error: function(){                      
            alert('Prišlo je do napake pri nalaganju podatkov. Prosimo poskusite ponovno!');
        }
  });
}

function ajax_load_transcriptions()
{
  var id_word=$("#words option:selected").val();
  var id_subject=$("#subjects option:selected").val();
  var id_location=$("#locations-select option:selected").val();

  $.ajax({
        type: "POST",
        url: "<?=$this->url?>ajax_load_transcriptions",
        cache: false,               
        data: {
          id_word: id_word,
          id_subject: id_subject,
          id_location:id_location,
        },
        dataType: "json",
        success: function(data){ 
            if(data.status=='ok')
            {
              var options;
              $('#titles').empty();

              //sort by name
              var sortable = [];
              for (var title in data.titles) {
                  sortable.push([title, data.titles[title]]);
              }
              var first=sortable.shift();
              sortable.sort(function(a, b) {
                  return a[1].localeCompare(b[1]);
              });
              sortable.unshift(first);
             
              $.each(sortable, function(id, title){
                  options += '<option value=' + title[0] + '>' + title[1] + '</option>';
              });
              
              $('#titles').append(options);

              //update table
              $('#transcriptions_table').html(data.view);
               
            } 
        },
        error: function(){                      
            alert('Prišlo je do napake pri nalaganju podatkov. Prosimo poskusite ponovno!');
        }
  });
}

function ajax_update_transcriptions_titles()
{
  var id_word=$("#words option:selected").val();
  var id_subject=$("#subjects option:selected").val();
  var id_lexem=$("#titles option:selected").val();
  var id_location=$("#locations-select option:selected").val();

  $.ajax({
        type: "POST",
        url: "<?=$this->url?>ajax_update_transcriptions_titles",
        cache: false,               
        data: {
          id_word:id_word,
          id_subject:id_subject,
          id_lexem:id_lexem,
          id_location:id_location,
        },
        dataType: "json",
        success: function(data){ 
            if(data.status=='ok')
            {
              //update table
              $('#transcriptions_table').html(data.view);
              
            } 
            else
            {

            }  
        },
        error: function(){                      
            alert('Prišlo je do napake pri nalaganju podatkov. Prosimo poskusite ponovno!');
        }
  });
}

function ajax_change_location()
{
  var id_word=$("#words option:selected").val();
  var id_subject=$("#subjects option:selected").val();
  var id_lexem=$("#titles option:selected").val();
  var id_location=$("#locations-select option:selected").val();
  
  $.ajax({
        type: "POST",
        url: "<?=$this->url?>ajax_change_location",
        cache: false,               
        data: {
          id_word:id_word,
          id_subject:id_subject,
          id_lexem:id_lexem,
          id_location:id_location,
        },
        dataType: "json",
        success: function(data){ 
            if(data.status=='ok')
            {
              //update table
              $('#transcriptions_table').html(data.view);
              
            } 
            else
            {

            }  
        },
        error: function(){                      
            alert('Prišlo je do napake pri nalaganju podatkov. Prosimo poskusite ponovno!');
        }
  });
}

function ajax_transcription_delete(id_transcription)
{

  $.ajax({
        type: "POST",
        url: "<?=$this->url?>ajax_transcription_delete",
        cache: false,               
        data:{
          id_transcription:id_transcription
        },
        dataType: "json",
        success: function(data){  
            if(data.status=='ok')
            { 
                //remove row
                $('#row-'+id_transcription).remove();
            }
            else
            {
              var errors='';
              $.each(data.errfields, function(i, error){
                  errors+=error+'\n';
              });
              status('error',errors);
            }      
        },
        error: function(){                      
            alert('Prišlo je do napake pri nalaganju podatkov. Prosimo poskusite ponovno!');
        }
  });
}

function status(status,text)
{
  $('#alert').fadeIn(400);
  //change div color
  if(status=='ok')
  {
    $('#alert').removeClass();
    $('#alert').addClass('alert alert-success');
    $('#alert_text').html("<span class='glyphicon glyphicon-ok text-success'></span> "+text);
  }
  else
  {
    $('#alert').removeClass();
    $('#alert').addClass('alert alert-danger');
    $('#alert_text').html("<span class='glyphicon glyphicon-exclamation-sign text-danger'></span> "+text);
  }
}

</script>