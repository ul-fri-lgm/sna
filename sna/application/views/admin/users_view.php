<?php

/*
Slovenski narečni atlas / Slovenian dialectal atlas
    Copyright (C) 2017  Gregor Šajn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

?>

<div class="container container-panel">
  <?
  $this->load->view('admin/index_view.php');
  ?>
  <div class="row" style="overflow: none">
    <?
    $this->load->view('admin/admin_nav.php');
    ?>
    
    <!-- tukaj se naloži poljuben pogled -->
    <div class="col-sm-9">
      <ol class="breadcrumb">
          <li><a href="<?=$this->url?>">Profil in uporabniki</a></li>
          <li><a href="<?=$this->url?>users">Uporabniki</a></li>
      </ol>
      <div class="row">
        <div class="col-sm-10"><h4 class="text-muted">Uporabniki</h4></div>
        <div class="col-sm-2 text-right"><a class="btn btn-success" href="<?=$this->url?>add_user" title="Dodaj uporabnika">Dodaj</a></div>
      </div>


      <br/>
      <div class="list">
      <table class="table table-striped table-hover">
      <thead>
        <tr>
          <th width="10%">Št.</th>
          <th>Uporabniško ime</th>
          <th>Ime</th>
          <th>Priimek</th>
          <th>Odstrani</th>
        </tr>
      </thead>
      <tbody>
        <?

        foreach($users as $i=>$user)
        {
          ?>
          <tr id="row-<?=$user['id']?>" onclick="window.document.location='<?=$this->url?>edit_user/<?=$user['id']?>'">
            <td><?=$i+1?>.</td>
            <td><?=$user['username']?></td>
            <td><?=$user['name']?$user['name']:'-'?></td>
            <td><?=$user['lastname']?$user['lastname']:'-'?></td>
            <?php if($_SESSION['username'] !== $user['username']): ?>
              <td><span class="glyphicon glyphicon-remove glyphicon-hover text-danger" onclick="if(confirm('Izbriši uporabnika <?php echo $user['username'] ?>?')){user_delete(<?=$user['id']?>)};event.stopPropagation();"></span></td>
            <?php else: ?>
                <td style="color: #818181">Prijavljen uporabnik</td>
            <?php endif; ?>
          </tr>
          <?
        }
        ?>
      </tbody>
      </table>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">
$('.close').click(function() {
    $('#alert_ok').fadeOut(500);
});

function user_delete(id_user)
{

    $.ajax({
        type: "POST",
        url: "<?=$this->url?>user_delete",
        cache: false,
        data:{
            id_user:id_user
        },
        dataType: "json",
        success: function(data){
            if(data.status==='ok')
            {
                //remove row
                $('#row-'+id_user).remove();
            }
            else
            {
                var errors='';
                $.each(data.errfields, function(i, error){
                    errors+=error+'\n';
                });
                status('error',errors);
            }
        },
        error: function(e){
            alert('Uporabnika ni bilo mogoče izbrisati!');
        }
    });
}

</script>