<?php

/*
Slovenski narečni atlas / Slovenian dialectal atlas
    Copyright (C) 2017  Gregor Šajn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

if(isset($words) and $words)
{
  foreach($words as $word)
  {
    ?>
    <div class="modal fade" id="modal_comment-<?=$word['id']?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content" style="height: 80%;">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel"><?=$word['title']?> komentar</h4>
          </div>
          <div class="modal-body text-center" id="modal_comment_content">
              <iframe id="modal_iframe" frameBorder="0" src="<?=base_url().$word['comment']?>#zoom=100"></iframe>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Zapri</button>
          </div>
        </div>
      </div>
    </div>
    <?
  }
}
?>


