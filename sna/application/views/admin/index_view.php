<?php

/*
Slovenski narečni atlas / Slovenian dialectal atlas
    Copyright (C) 2017  Gregor Šajn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

?>

<div class="page-header">
	<div class="row">
		<div class="col-sm-9">
			<h3>Nadzorna plošča</h3>
		</div>
		<div class="col-sm-3 text-right">
			<div class="btn-group" style="margin-top:20px;">
				<a href="<?=base_url()?>" target="_blank" class="btn btn-default" title="Na prvo stran">Nazaj na karto</a>
				<a href="<?=$this->url?>logout" class="btn btn-default confirm-logout" title="Odjava">Odjava <span class="glyphicon glyphicon-off"></span></a>

			</div>
		</div>
	</div>
</div>


<script type="text/javascript">
$('.confirm-logout').click(function() {
      return window.confirm("Odjava?");
});

</script>


