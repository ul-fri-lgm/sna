<?php

/*
Slovenski narečni atlas / Slovenian dialectal atlas
    Copyright (C) 2017  Gregor Šajn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

?>

<div class="container container-panel">
  <?
  $this->load->view('admin/index_view.php');
  ?>
  <div class="row" style="overflow: none">
    <?
    $this->load->view('admin/admin_nav.php');
    ?>
    
    <!-- tukaj se naloži poljuben pogled -->
    <div class="col-sm-9">
      <ol class="breadcrumb">
          <li><a href="<?=$this->url?>transcriptions">Transkripcije</a></li>
          <li><a href="<?=$this->url?>edit_transcription/<?=$transcription['id']?>"><?=$transcription['title']?></a></li>
      </ol>
      <div class="row">
        <div class="col-sm-9"><h4 class="text-muted"><?=$transcription['title']?></h4></div>
        <div class="col-sm-3 text-right">
        <a class="btn btn-danger confirm" href="<?=$this->url?>location_delete/<?=$location['id']?>">Odstrani</a></div>
      </div>
      <br>
      <!-- alerts -->
      <?
      if(isset($errfields) and $errfields)
      {
          $alert_text='<ul>';
          foreach($errfields as $errfield)
          {
            $alert_text.='<li>'.$errfield.'</li>';
          }
          $alert_text.='</ul>'
          ?>
          <div class="alert alert-danger fade in" id="alert_err" role="alert">
          <button type="button" class="close">
            <span aria-hidden="true">&times;</span>
          </button>
          <span class="glyphicon glyphicon-exclamation-sign text-danger"></span> <?=$alert_text?>         
          </div>
          <?
      }
      elseif(isset($status) and $status=='ok')
      {
        ?>
        <div class="alert alert-success fade in" id="alert_ok" role="alert">
        <button type="button" class="close">
            <span aria-hidden="true">&times;</span>
        </button>
        <span class="text-success glyphicon glyphicon-ok"></span> Spremembe uspešno shranjene!
        </div>
        <?
      }
      ?>

       <form class="form-horizontal form-border" action="<?=$this->url?>post_edit_location" method="POST" id="edit_subject">
        <input type="hidden" id="id" name="id" value="<?=$transcription['id']?>">
        <div class="form-group">
          <label for="name" class="col-sm-2 control-label">Kraj</label>
          <div class="col-sm-10">
            <input type="text" class="form-control input-md" value="<?=$location['name']?>" id="name" name="name">
          </div>
        </div>
        <div class="form-group">
          <label for="short_name" class="col-sm-2 control-label">Kratek naziv</label>
          <div class="col-sm-10">
            <input type="text" class="form-control input-md" value="<?=$location['short_name']?>" id="short_name" name="short_name">
          </div>
        </div>
        <div class="form-group">
          <label for="short_name" class="col-sm-2 control-label">X koordinata</label>
          <div class="col-sm-10">
            <input type="text" class="form-control input-md input-short" value="<?=$location['x']?>" id="x" name="x">
          </div>
        </div>
        <div class="form-group">
          <label for="short_name" class="col-sm-2 control-label">Y koordinata</label>
          <div class="col-sm-10">
            <input type="text" width="100px" class="form-control input-md input-short" value="<?=$location['y']?>" id="y" name="y" >
          </div>
        </div>
        <br>
        <div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-success button-medium">Shrani</button>
            <button type="submit" class="btn btn-default button-medium" name=save_close>Shrani in zapri</button>
          </div>
        </div>
      </form>
      <br>
    </div>
  </div>
</div>

<script type="text/javascript">
$('.close').click(function() {
    $('#alert_err').fadeOut(500);
    $('#alert_ok').fadeOut(500);
});

$('.confirm').click(function() {
      return window.confirm("Izbriši lokacijo?");
});

</script>