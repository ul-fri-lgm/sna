<?php

/*
Slovenski narečni atlas / Slovenian dialectal atlas
    Copyright (C) 2017  Gregor Šajn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

?>

<table class="table table-striped table-hover">
  <thead>
    <tr>
      <th width="5%">Št.</th>
      <th width="12%">Leksem</th>
      <th width="12%">Fonetični zapis</th>
      <th width="16%" class="text-center">Zvočni posnetek</th>
      <th width="32%">Beseda (tema)</th>
      <th width="20%">Kraj</th>
      <th width="5%" class="text-center">Odstrani</th>
    </tr>
  </thead>
  <tbody>
    <?
    foreach($transcriptions as $i=>$transcription)
    {
      ?>
      <tr id="row-<?=$transcription['id']?>"onclick="window.document.location='<?=$this->url?>edit_transcription/<?=$transcription['id']?>'">
        <td><?=$i+1?>.</td>
        <td><?=$transcription['lexem_star'] === '1' ? $transcription['lexem_title'] . '*' : $transcription['lexem_title']?></td>
        <td class="ZRCola"><?=$transcription['phonetic_writing']?></td>
        <td class="text-center">
        <?
        if($transcription['audio'])
        {
          ?>
          <audio id="player-<?=$transcription['id']?>" src="<?php echo base_url().$transcription['audio']?>"></audio>
          <div>
            <button class="glyphicon glyphicon-play-circle btn btn-default btn-xs" onclick="event.stopPropagation();document.getElementById('player-<?=$transcription['id']?>').play();"></button>
            <button class="glyphicon glyphicon-pause btn btn-default btn-xs" onclick="event.stopPropagation();document.getElementById('player-<?=$transcription['id']?>').pause()"></button>
            <button class="glyphicon glyphicon-stop btn btn-default btn-xs" onclick="event.stopPropagation();document.getElementById('player-<?=$transcription['id']?>').pause();document.getElementById('player-<?=$transcription['id']?>').currentTime=0.0;"></button>
          </div>
          <?
        }
        else
        {
          echo "-";
        }
        ?>

         
        <td>
        <?
        if($transcription['word_title'])
        {
          echo $transcription['word_title']?> (<?=$transcription['subject_title']?>)<?
        }
        else
        {
          ?>-<?
        }
        ?>
        </td>
        <td><?
        if($transcription['name'])
        {
          echo $transcription['name'];
        }
        else
        {
          ?>-<?
        }
        ?>
        </td>
        <td class="text-center"><span class="glyphicon glyphicon-hover glyphicon-remove text-danger" onclick="if(confirm('Izbriši transkripcijo <?php echo $transcription['lexem_title'] ?>?')){ajax_transcription_delete(<?=$transcription['id']?>)};event.stopPropagation();"></span></td>
      </tr>
      <?
    }
    ?>
  </tbody>
</table>


