<?php

/*
Slovenski narečni atlas / Slovenian dialectal atlas
    Copyright (C) 2017  Gregor Šajn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

?>

<div class="container container-panel">
  <?
  $this->load->view('admin/index_view.php');
  ?>
  <div class="row" style="overflow: none">
    <?
    $this->load->view('admin/admin_nav.php');
    ?>
    
    <div class="col-sm-9">
      <ol class="breadcrumb">
          <li><a href="<?=$this->url?>">Profil in uporabniki</a></li>
      </ol>
      <div class="row">
       <div class="col-sm-9">
          <h4 class="text-muted">Urejanje uporabnika - sprememba gesla</h4>
        </div>
        <div class="col-sm-3 text-right">
        <?
        if($this->session->userdata('uid')==1)
        {
          ?>
          <a class="btn btn-primary" href="<?=$this->url?>users" title="Pregled uporabnikov">Pregled uporabnikov</a>
          <?
        }
        
        ?>
        </div>
      </div>
      <br/>

      <?
      if(isset($errfields) and $errfields)
        {
            $alert_text='<ul>';
            foreach($errfields as $errfield)
            {
              $alert_text.='<li>'.$errfield.'</li>';
            }
            $alert_text.='</ul>'
            ?>
            <div class="alert alert-danger fade in" id="alert_err" role="alert">
            <button type="button" class="close">
              <span aria-hidden="true">&times;</span>
            </button>
            <span class="glyphicon glyphicon-exclamation-sign text-danger"></span> <?=$alert_text?>
            </div>
            <?
        }
        elseif(isset($status) and $status=='ok')
        {
          ?>
          <div class="alert alert-success fade in" id="alert_ok" role="alert">
          <button type="button" class="close">
              <span aria-hidden="true">&times;</span>
          </button>
          <span class="text-success glyphicon glyphicon-ok"></span> Spremembe uspešno shranjene!
          </div>
          <?
        }
      ?>
     
      <div class="alert alert-info dismissable" id="alert_info"><button type="button" class="close dismiss">
            <span aria-hidden="true">&times;</span>
          </button><span class="glyphicon glyphicon-info-sign"></span> Če želite, lahko tu spremenite svoje geslo. Sprememba gesla ni obvezna.</div>
      <form class="form-horizontal form-border" action="<?=$this->url?>post_edit_my_profile" method="POST" id="edit_personal_data">
        
        <input type="hidden" id="id_user" name="id_user" value="<?=$user['id']?>">
        <div class="form-group">
          <label for="ime" class="col-sm-3 control-label">Geslo</label>
          <div class="col-sm-9">
            <input type="password" class="form-control input-md" value="" id="pass1" name="pass1">
          </div>
        </div>
        <div class="form-group">
          <label for="lastname" class="col-sm-3 control-label">Ponovi geslo</label>
          <div class="col-sm-9">
            <input type="password" class="form-control input-md" value="" id="pass2" name="pass2" >
          </div>
        </div>
        
        <div class="form-group">
          <div class="col-sm-offset-3 col-sm-9">
            <button type="submit" class="btn btn-success">Shrani</button>
          </div>
        </div>

      </form>
       

    </div>
  </div>
</div>

<script type="text/javascript">
$('.close').click(function() {
    $('#alert_err').fadeOut(500);
    $('#alert_ok').fadeOut(500);
});

$('.dismiss').click(function() {

    $('#alert_info').fadeOut(500);
});

$('.dismissable').delay(30000).fadeOut(500);


</script>