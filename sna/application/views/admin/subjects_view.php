<?php

/*
Slovenski narečni atlas / Slovenian dialectal atlas
    Copyright (C) 2017  Gregor Šajn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

?>

<div class="container container-panel">
  <?
  $this->load->view('admin/index_view.php');
  ?>
  <div class="row" style="overflow: none">
    <?
    $this->load->view('admin/admin_nav.php');
    ?>
    
    <!-- tukaj se naloži poljuben pogled -->
    <div class="col-sm-9">
      <ol class="breadcrumb">
          <li><a href="<?=$this->url?>edit_content">Teme</a></li>
      </ol>
      <div class="row">
        <div class="col-sm-10"><h4 class="text-muted">Teme</h4></div>
        <div class="col-sm-2 text-right"><a class="btn btn-success" href="<?=$this->url?>add_subject" title="Dodaj temo">Dodaj</a></div>
      </div>
      <?
      

      if(isset($status) and $status=='ok')
      {
          ?>
          <div class="alert alert-success fade in" id="alert_ok" role="alert">
          <button type="button" class="close">
            <span aria-hidden="true">&times;</span>
          </button>
          <span class="glyphicon glyphicon-ok text-success"></span> Tema uspešno izbrisana!
          </div>
          <?
      }
      ?>

      <br/>
      <div class="list">
      <table class="table table-striped table-hover">
      <thead>
        <tr>
          <th width="10%">Št.</th>
          <th>Tema</th>
        </tr>
      </thead>
      <tbody>
        <?

        foreach($subjects as $i=>$subject)
        {
          ?>
          <tr onclick="window.document.location='<?=$this->url?>edit_subject/<?=$subject['id']?>'">
            <td><?=$i+1?>.</td>
            <td><?=$subject['title']?></td>
          </tr>
          <?
        }
        ?>
      </tbody>
      </table>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">
$('.close').click(function() {
    $('#alert_ok').fadeOut(500);
});

</script>