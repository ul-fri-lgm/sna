<?php

/*
Slovenski narečni atlas / Slovenian dialectal atlas
    Copyright (C) 2017  Gregor Šajn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

?>

<div class="container container-panel">
  <?
  $this->load->view('admin/index_view.php');
  ?>
  <div class="row" style="overflow: none">
    <?
    $this->load->view('admin/admin_nav.php');
    ?>
    
    <!-- tukaj se naloži poljuben pogled -->
    <div class="col-sm-9">
      <ol class="breadcrumb">
          <li><a href="<?=$this->url?>locations">Lokacije</a></li>
          <li><a href="#">Nova lokacija</a></li>
      </ol>
      <div class="row">
        <div class="col-sm-9"><h4 class="text-muted">Nova lokacija</h4></div>
      </div>
      <br>
      <!-- alerts -->
      <?
      if(isset($errfields) and $errfields)
      {
          $alert_text='<ul>';
          foreach($errfields as $errfield)
          {
            $alert_text.='<li>'.$errfield.'</li>';
          }
          $alert_text.='</ul>'
          ?>
          <div class="alert alert-danger fade in" id="alert_err" role="alert">
          <button type="button" class="close">
            <span aria-hidden="true">&times;</span>
          </button>
          <span class="glyphicon glyphicon-exclamation-sign text-danger"></span> <?=$alert_text?>         
          </div>
          <?
      }
      ?>

       <form class="form-horizontal form-border" action="<?=$this->url?>post_add_location" method="POST" id="edit_location">
        <div class="form-group">
          <label for="name" class="col-sm-2 control-label">Kraj<span class="text-danger">*</span></label>
          <div class="col-sm-10">
            <input type="text" class="form-control input-md" value="<?=$location['name']?>" id="name" name="name">
          </div>
        </div>
        <div class="form-group">
          <label for="short_name" class="col-sm-2 control-label">Kratek naziv<span class="text-danger">*</span></label>
          <div class="col-sm-10">
            <input type="text" class="form-control input-md" value="<?=$location['short_name']?>" id="short_name" name="short_name">
          </div>
        </div>
           <div class="form-group">
               <label for="subjects" class="col-sm-2 control-label">Tema<span class="text-danger">*</span></label>
               <div class="col-sm-10">
                   <select class="form-control" style="width:315px;" id="subject" name="subject">
                       <?
                       foreach($subjects as $id=>$subject)
                       {
                           ?><option value="<?=$id?>" <?=$id==$location['id_subject']?'selected="selected"':''?>><?=$subject?></option><?
                       }
                       ?>
                   </select>
               </div>
           </div>
        <div class="form-group">
          <label for="short_name" class="col-sm-2 control-label">Zemljepisna širina (lat)<span class="text-danger">*</span></label>
          <div class="col-sm-10">
            <input type="text" style="width:120px;" class="form-control input-md input-short" value="<?=$location['lat']?>" id="lat" name="lat">
          </div>
        </div>
        <div class="form-group">
          <label for="short_name" class="col-sm-2 control-label">Zemljepisna dolžina (long)<span class="text-danger">*</span></label>
          <div class="col-sm-10">
            <input type="text" style="width:120px;" class="form-control input-md input-short" value="<?=$location['long']?>" id="long" name="long" >
          </div>
        </div>
        <div class="form-group">
          <label for="dialects" class="col-sm-2 control-label">Narečna skupina<span class="text-danger">*</span></label>
          <div class="col-sm-10">
              <select class="form-control" style="width:300px;" id="dialects" name="dialects" onchange="ajax_update_subdialects();">
              <? 
              foreach($dialects as $id=>$dialect)
              {
                ?><option value="<?=$id?>" <?=$id==$location['id_dialect']?'selected="selected"':''?>><?=$dialect?></option><?
              }
              ?>
              </select>
          </div>
        </div>
        <div class="form-group">
          <label for="subdialects" class="col-sm-2 control-label">Narečje<span class="text-danger">*</span></label>
          <div class="col-sm-10">
              <select class="form-control" style="width:300px;" id="subdialects" name="subdialects" onchange="ajax_update_subsubdialects();">
              <? 
              foreach($subdialects as $id=>$dialect)
              {
                ?><option value="<?=$id?>" <?=$id==$location['id_subdialect']?'selected="selected"':''?>><?=$dialect?></option><?
              }
              ?>
              </select>
          </div>
        </div>
        <div class="form-group">
          <label for="subsubdialects" class="col-sm-2 control-label">Podnarečje</label>
          <div class="col-sm-10">
              <select class="form-control" style="width:300px;" id="subsubdialects" name="subsubdialects">
              <? 
              foreach($subsubdialects as $id=>$dialect)
              {
                ?><option value="<?=$id?>" <?=$id==$location['id_subsubdialect']?'selected="selected"':''?>><?=$dialect?></option><?
              }
              ?>
              </select>
          </div>
        </div>
        <br>
        <div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-success button-medium">Shrani</button>
            <button type="submit" class="btn btn-default button-medium" name=save_close>Shrani in zapri</button>
          </div>
        </div>
      </form>
      <br>
    </div>
  </div>
</div>

<script type="text/javascript">
$('.close').click(function() {
    $('#alert_err').fadeOut(500);
});

function ajax_update_subdialects()
{
  var id_dialect=$("#dialects option:selected").val();

  $.ajax({
        type: "POST",
        url: "<?=$this->url?>ajax_update_subdialects",
        cache: false,               
        data: {
          id_dialect: id_dialect
        },
        dataType: "json",
        success: function(data){ 
          if(data.status=='ok')
          {
            var options;
            $('#subdialects').empty();
           
            $.each(data.subdialects, function(id, title){
                options += '<option value=' + id + '>' + title + '</option>';
            });
            
            $('#subdialects').append(options);

          }    
        },
        error: function(){                      
            alert('Prišlo je do napake pri nalaganju podatkov. Prosimo poskusite ponovno!');
        }
  });
}

function ajax_update_subsubdialects()
{
  var id_subdialect=$("#subdialects option:selected").val();

  $.ajax({
        type: "POST",
        url: "<?=$this->url?>ajax_update_subsubdialects",
        cache: false,               
        data: {
          id_subdialect: id_subdialect
        },
        dataType: "json",
        success: function(data){ 
          if(data.status=='ok')
          {
            var options;
            $('#subsubdialects').empty();
           
            $.each(data.subsubdialects, function(id, title){
                options += '<option value=' + id + '>' + title + '</option>';
            });
            
            $('#subsubdialects').append(options);

          }    
        },
        error: function(){                      
            alert('Prišlo je do napake pri nalaganju podatkov. Prosimo poskusite ponovno!');
        }
  });
}




</script>