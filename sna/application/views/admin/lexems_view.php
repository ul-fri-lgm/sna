<?php

/*
Slovenski narečni atlas / Slovenian dialectal atlas
    Copyright (C) 2017  Gregor Šajn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

?>

<div class="container container-panel">
  <?
  $this->load->view('admin/index_view.php');
  ?>
  <div class="row" style="overflow: none">
    <?
    $this->load->view('admin/admin_nav.php');
    ?>
    <!-- tukaj se naloži poljuben pogled -->
    <div class="col-sm-9">
      <ol class="breadcrumb">
          <li><a href="<?=$this->url?>lexems">Leksemi</a></li>
      </ol>
      <div class="row">
        <div class="col-sm-10"><h4 class="text-muted">Leksemi</h4></div>
        <div class="col-sm-2 text-right"><a class="btn btn-success" href="<?=$this->url?>add_lexem" title="Dodaj leksem">Dodaj</a></div>
      </div>
      <!-- alerts -->
      <?
      if(isset($status) and $status=='ok')
      {
        ?>
        <div class="alert alert-success fade in" id="alert_ok" role="alert">
        <button type="button" class="close">
            <span aria-hidden="true">&times;</span>
          </button>
        <span class="text-success glyphicon glyphicon-ok"></span> Spremembe uspešno shranjene!
        </div>
        <?
      }
      ?>
      <div class="alert fade in hidden" id="alert" role="alert">
      <button type="button" class="close" onclick="hide_alert()">
        <span aria-hidden="true">&times;</span>
      </button>
        <span id="alert_text"></span>
      </div>
      <br>


      <div class="form-inline">
        <div class="form-group">
            <label for="subjects" class="control-label">Tema</label>

              <select class="form-control input-sm" id="subjects" style="width:200px;" onchange="ajax_change_subject_lexems();">
              <? 
              foreach($subjects as $id=>$subject)
              {
                ?><option value="<?=$id?>" <?=$id==$id_subject?'selected="selected"':''?>><?=$subject?></option><?
              }
              ?>
              </select>
         <br>   
        </div>
        <div class="form-group">
            <label for="words" class="control-label">Beseda</label>

              <select class="form-control input-sm" id="words" style="width:200px;" onchange="ajax_change_word_lexems();">
              <? 
              foreach($words as $id=>$word)
              {
                ?><option value="<?=$id?>"><?=$word?></option><?
              }
              ?>
              </select>
            <br>
        </div>
      <br>
      <br>


      <div id="lexems_table" class="list">
      <?
      if(isset($view))
      {
        echo $view;
      }
      ?>
      </div>

    </div>
  </div>
</div>


<script type="text/javascript">
$('.close').click(function() {
    $('#alert_ok').fadeOut(500);
});

function ajax_lexem_delete(id_lexem)
{

  $.ajax({
        type: "POST",
        url: "<?=$this->url?>ajax_lexem_delete",
        cache: false,               
        data:{
          id_lexem:id_lexem
        },
        dataType: "json",
        success: function(data){  
            if(data.status=='ok')
            { 
                //remove row
                $('#row-'+id_lexem).remove();
            }
            else
            {
              var errors='';
              $.each(data.errfields, function(i, error){
                  errors+=error+'\n';
              });
              status('error',errors);
            }      
        },
        error: function(){                      
            alert('Prišlo je do napake pri nalaganju podatkov. Prosimo poskusite ponovno!');
        }
  });
}


function status(status,text)
{
  $('#alert').fadeIn(400);
  //change div color
  if(status=='ok')
  {
    $('#alert').removeClass();
    $('#alert').addClass('alert alert-success');
    $('#alert_text').html("<span class='glyphicon glyphicon-ok text-success'></span> "+text);
  }
  else
  {
    $('#alert').removeClass();
    $('#alert').addClass('alert alert-danger');
    $('#alert_text').html("<span class='glyphicon glyphicon-exclamation-sign text-danger'></span> "+text);
  }
}

function hide_alert()
{
  $('#alert').fadeOut(500);
  $('alert').addClass('hidden'); 
}


function ajax_change_subject_lexems()
{
  var id_subject=$("#subjects option:selected").val();

  $.ajax({
        type: "POST",
        url: "<?=$this->url?>ajax_change_subject_lexems",
        cache: false,               
        data: {
          id_subject: id_subject,
        },
        dataType: "json",
        success: function(data){ 
          if(data.status=='ok')
          {
            var options;
            $('#words').empty();
           
            $.each(data.words, function(id, title){
                options += '<option value=' + id + '>' + title + '</option>';
            });
            
            $('#words').append(options);

            //update table
            $('#lexems_table').html(data.view);

          }    
        },
        error: function(){                      
            alert('Prišlo je do napake pri nalaganju podatkov. Prosimo poskusite ponovno!');
        }
  });
}

function ajax_change_word_lexems()
{
  var id_word=$("#words option:selected").val();
  var id_subject=$("#subjects option:selected").val();

  $.ajax({
        type: "POST",
        url: "<?=$this->url?>ajax_change_word_lexems",
        cache: false,               
        data: {
          id_word: id_word,
          id_subject: id_subject,
        },
        dataType: "json",
        success: function(data){ 
            if(data.status=='ok')
            {
              var options;
            $('#titles').empty();
           
            $.each(data.titles, function(id, title){
                options += '<option value=' + id + '>' + title + '</option>';
            });
            
            //update table
            $('#lexems_table').html(data.view);
               
            } 
        },
        error: function(){                      
            alert('Prišlo je do napake pri nalaganju podatkov. Prosimo poskusite ponovno!');
        }
  });
}


</script>