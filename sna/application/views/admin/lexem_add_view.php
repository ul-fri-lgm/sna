<?php

/*
Slovenski narečni atlas / Slovenian dialectal atlas
    Copyright (C) 2017  Gregor Šajn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

?>

<div class="container container-panel">
  <?
  $this->load->view('admin/index_view.php');
  ?>
  <div class="row" style="overflow: none">
    <?
    $this->load->view('admin/admin_nav.php');
    ?>
    
    <!-- tukaj se naloži poljuben pogled -->
    <div class="col-sm-9">
      <ol class="breadcrumb">
          <li><a href="<?=$this->url?>lexems">Leksemi</a></li>
          <li><a href="#">Nov leksem</a></li>
      </ol>
      <div class="row">
        <div class="col-sm-9"><h4 class="text-muted">Nov leksem</h4></div>
      </div>
      <br>
      <!-- alerts -->
      <?
      if(isset($errfields) and $errfields)
      {
          $alert_text='<ul>';
          foreach($errfields as $errfield)
          {
            $alert_text.='<li>'.$errfield.'</li>';
          }
          $alert_text.='</ul>'
          ?>
          <div class="alert alert-danger fade in" id="alert_err" role="alert">
          <button type="button" class="close">
            <span aria-hidden="true">&times;</span>
          </button>
          <span class="glyphicon glyphicon-exclamation-sign text-danger"></span> <?=$alert_text?>         
          </div>
          <?
      }
      ?>

       <form class="form-horizontal form-border" action="<?=$this->url?>post_add_lexem" method="POST" id="add_lexem">
        <div class="form-group">
          <label for="subjects" class="col-sm-2 control-label">Tema</label>
          <div class="col-sm-10">
              <select class="form-control" style="width:315px;" id="subjects" name="subjects" onchange="ajax_update_words();">
              <? 
              foreach($subjects as $id=>$subject)
              {
                ?><option value="<?=$id?>" <?=$id==$lexem['id_subject']?'selected="selected"':''?>><?=$subject?></option><?
              }
              ?>
              </select>
          </div>
        </div>
        <div class="form-group">
          <label for="words" class="col-sm-2 control-label">Beseda<span class="text-danger">*</span></label>
          <div class="col-sm-10">
              <select class="form-control" style="width:315px;" name="words" id="words">
              <? 
              foreach($words as $id=>$word)
              {
                ?><option value="<?=$id?>" <?=$id==$lexem['id_word']?'selected="selected"':''?>><?=$word?></option><?
              }
              ?>
              </select>
          </div>
        </div>
        <div class="form-group">
          <label for="title" class="col-sm-2 control-label">Leksem<span class="text-danger">*</span></label>
          <div class="col-sm-10">
            <input type="text" class="form-control input-md" value="<?=$lexem['title']?>" id="title" name="title" style="width:315px;">
          </div>
        </div>
           <div class="form-group">
               <label for="star" class="col-sm-2 control-label">Leksem ima simbol * (zvezdica)</label>
               <div class="col-sm-2">
                   <label class="switch">
                       <input type="checkbox" <? if($lexem['star'] === '1' || $lexem['star'] === 1) echo 'checked'?> id="star" name="star">
                       <span class="slider round"></span>
                   </label>
               </div>
           </div>
           <div id="div1" class="form-group form-inline">
               <label for="icon1" class="col-sm-2 control-label">Prvi simbol leksema<span class="text-danger">*</span></label>
               <div class="col-sm-10">
                   <input type="text" class="form-control input-md SIMBola" value="<?=$lexem['icon1']?>" id="icon1" name="icon1" style="margin-right: 10px; width:100px;">
                   <label for="icon_color1" control-label">Barva</label>
                   <input style="margin-right: 10px;" name="icon_color1" id="icon_color1" value="<?=$lexem['icon_color1']?>" type="color"/>
                   <label for="icon_size1" control-label">Velikost</label>
                   <select class="dropdown" name="icon_size1" id="icon_size1" title="Izberite velikost">
                       <option selected id="normal1" value="normal">Normalna</option>
                       <option <? if($lexem['icon_size1'] === 'small') echo 'selected' ?> id="small1" value="small">Majhna</option>
                   </select>
               </div>
           </div>
           <div  id="div2" class="form-group form-inline">
               <label for="icon2" class="col-sm-2 control-label">Drugi simbol leksema</label>
               <div class="col-sm-10">
                   <input type="text" class="form-control input-md SIMBola" value="<?=$lexem['icon2']?>" id="icon2" name="icon2" style="margin-right: 10px; width:100px;">
                   <label for="icon_color2" control-label">Barva</label>
                   <input style="margin-right: 10px;" name="icon_color2" id="icon_color2" value="<?=$lexem['icon_color2']?>" type="color"/>
                   <label for="icon_size2" control-label">Velikost</label>
                   <select class="dropdown" name="icon_size2" id="icon_size2" title="Izberite velikost">
                       <option selected id="normal2" value="normal">Normalna</option>
                       <option <? if($lexem['icon_size2'] === 'small') echo 'selected' ?> id="small2" value="small">Majhna</option>
                   </select>
               </div>
           </div>
           <div  id="div3" class="form-group form-inline">
               <label for="icon3" class="col-sm-2 control-label">Tretji simbol leksema</label>
               <div class="col-sm-10">
                   <input type="text" class="form-control input-md SIMBola" value="<?=$lexem['icon3']?>" id="icon3" name="icon3" style="margin-right: 10px; width:100px;">
                   <label for="title" control-label">Barva</label>
                   <input style="margin-right: 10px;" name="icon_color3" id="icon_color3" value="<?=$lexem['icon_color3']?>" type="color"/>
                   <label for="title" control-label">Velikost</label>
                   <select class="dropdown" name="icon_size3" id="icon_size3" title="Izberite velikost">
                       <option selected id="normal3" value="normal">Normalna</option>
                       <option <? if($lexem['icon_size3'] === 'small') echo 'selected' ?> id="small3" value="small">Majhna</option>
                   </select>
               </div>
           </div>
           <div  id="div4" class="form-group form-inline   ">
               <label for="icon4" class="col-sm-2 control-label">Četrti simbol leksema</label>
               <div class="col-sm-10">
                   <input type="text" class="form-control input-md SIMBola" value="<?=$lexem['icon4']?>" id="icon4" name="icon4" style="margin-right: 10px; width:100px;">
                   <label for="title" control-label">Barva</label>
                   <input style="margin-right: 10px;" name="icon_color4" id="icon_color4" value="<?=$lexem['icon_color4']?>" type="color"/>
                   <label for="title" control-label">Velikost</label>
                   <select class="dropdown" name="icon_size4" id="icon_size4" title="Izberite velikost">
                       <option selected id="normal4" value="normal">Normalna</option>
                       <option <? if($lexem['icon_size4'] === 'small') echo 'selected' ?> id="small4" value="small">Majhna</option>
                   </select>
               </div>
           </div>
        <br>        <div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-success button-medium">Shrani</button>
            <button type="submit" class="btn btn-default button-medium" name=save_close>Shrani in zapri</button>
          </div>
        </div>
      </form>
      <br>
    </div>
  </div>
</div>

<script type="text/javascript">
$('.close').click(function() {
    $('#alert_err').fadeOut(500);
    $('#alert_ok').fadeOut(500);
});

function ajax_update_words()
{
  var id_subject=$("#subjects option:selected").val();

  $.ajax({
        type: "POST",
        url: "<?=$this->url?>ajax_update_words",
        cache: false,               
        data: {
          id_subject: id_subject
        },
        dataType: "json",
        success: function(data){ 
          if(data.status=='ok')
          {
            var options;
            $('#words').empty();
           
            $.each(data.words, function(id, title){
                options += '<option value=' + id + '>' + title + '</option>';
            });
            
            $('#words').append(options);

          }    
        },
        error: function(){                      
            alert('Prišlo je do napake pri nalaganju podatkov. Prosimo poskusite ponovno!');
        }
  });
}


</script>