<?php

/*
Slovenski narečni atlas / Slovenian dialectal atlas
    Copyright (C) 2017  Gregor Šajn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

?>

<div class="col-sm-3">
  <ul class="nav nav-pills nav-stacked nav-bordered">
    <li role="presentation" class="<?=($tab=='my_profile')?'active':''?>" id="my_profile"><a href="<?=$this->url?>">Profil in uporabniki</a></li>
    <li role="presentation" class="<?=($tab=='subjects_words')?'active':''?>" id="content"><a href="<?=$this->url?>edit_content">Teme in besede</a></li>
    <li role="presentation" class="<?=($tab=='locations')?'active':''?>" id="locations"><a href="<?=$this->url?>locations">Lokacije</a></li>
    <li role="presentation" class="<?=($tab=='lexems')?'active':''?>" id="lexems"><a href="<?=$this->url?>lexems">Leksemi</a></li>
    <li role="presentation" class="<?=($tab=='transcriptions')?'active':''?>" id="transcriptions"><a href="<?=$this->url?>transcriptions">Fonetični zapisi</a></li>
  </ul>
</div>