<?php

/*
Slovenski narečni atlas / Slovenian dialectal atlas
    Copyright (C) 2017  Gregor Šajn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

?>

<div class="container container-panel">
  <?
  $this->load->view('admin/index_view.php');
  ?>
  <div class="row" style="overflow: none">
    <?
    $this->load->view('admin/admin_nav.php');
    ?>
    
    <!-- tukaj se naloži poljuben pogled -->
    <div class="col-sm-9">
      <ol class="breadcrumb">
        <li><a href="<?=$this->url?>edit_content">Teme</a></li>
        <li><a href="<?=$this->url?>edit_subject/<?=$subject['id']?>"><?=$subject['title']?></a></li>
      </ol>
      <div class="row">
        <div class="col-sm-9"><h4 class="text-muted"><?=$subject['title']?></h4></div>
        <div class="col-sm-3 text-right">
         <a class="btn btn-success" href="<?=$this->url?>add_word/<?=$subject['id']?>" title="Dodaj besedo">Dodaj</a>
        <a class="btn btn-danger confirm" href="<?=$this->url?>subject_delete/<?=$subject['id']?>" title="Odstrani temo">Odstrani</a></div>
      </div>
      <br>
      <!-- alerts -->
      <?
      if(isset($errfields) and $errfields)
      {
          $alert_text='<ul>';
          foreach($errfields as $errfield)
          {
            $alert_text.='<li>'.$errfield.'</li>';
          }
          $alert_text.='</ul>'
          ?>
          <div class="alert alert-danger fade in" id="alert_err" role="alert">
          <button type="button" class="close">
            <span aria-hidden="true">&times;</span>
          </button>
          <span class="glyphicon glyphicon-exclamation-sign text-danger"></span> <?=$alert_text?>
          </div>
          <?
      }
      elseif(isset($status) and $status=='ok')
      {
        ?>
        <div class="alert alert-success fade in" id="alert_ok" role="alert">
        <button type="button" class="close">
            <span aria-hidden="true">&times;</span>
        </button>
        <span class="text-success glyphicon glyphicon-ok"></span> Spremembe uspešno shranjene!
        </div>
        <?
      }
      ?>

      <div class="alert fade in hidden" id="alert" role="alert">
        <button type="button" class="close">
            <span aria-hidden="true">&times;</span>
        </button>
        <span id="alert_text"></span>
      </div>
      
      <form class="form-horizontal form-border" action="javascript:;" onsubmit="ajax_edit_subject_save();" id="edit_subject">
        <input type="hidden" id="id" name="id" value="<?=$subject['id']?>">
        <div class="form-group">
          <label for="title" class="col-sm-2 control-label">Naziv teme<span class="text-danger">*</span></label>
          <div class="col-sm-10">
            <input type="text" class="form-control input-md" value="<?=$subject['title']?>" id="title" name="title">
          </div>
        </div>
        
        <div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-success button-medium">Shrani</button>
          </div>
        </div>
      </form>
      <br>
      <div class="list">
      <table class="table table-striped table-hover">
      <thead>
        <tr>
          <th width="10%">Št.</th>
          <th width="30%">Beseda</th>
          <th width="25%" class="text-center">Slika</th>
          <th width="25%" class="text-center">Komentar</th>
          <th width="10%" class="text-center">Odstrani</th>
        </tr>
      </thead>
      <tbody>
      <?
      foreach($words as $i=>$word)
      {
        ?>
          <tr id="row-<?=$word['id']?>" onclick="window.document.location='<?=$this->url?>edit_word/<?=$word['id']?>'">
            <td><?=$i+1?>.</td>
            <td><?=$word['title']?></td>
            <td class="text-center">
            <?
            if($word['image'])
            {
              ?>
              <span class="glyphicon glyphicon-picture" onclick="open_modal('<?=$word['modal_image_target']?>');" ></span>
              <?
            }
            else
            {
              ?>
              -
              <?
            }
            ?>
            </td>
            <td class="text-center">
            <?
            if($word['comment'])
            {
              ?>
              <span class="glyphicon glyphicon-book" onclick="open_modal('<?=$word['modal_comment_target']?>')"></span>
              <?
            }
            else
            {
              ?>
              -
              <?
            }
            ?>
            </td>
            <td class="text-center"><span class="glyphicon glyphicon-hover glyphicon-remove text-danger" onclick="if(confirm('Izbriši besedo <?php echo $word['title'] ?>?')){ajax_word_delete(<?=$word['id']?>)};event.stopPropagation();"></span></td>
          </tr>
        <?
      }
      ?> 
      </tbody>
      </table>
      </div>
    </div>
  </div>
</div>

<div id="modals_images">
<?=$view_images;?>
</div>
<div id="modals_comments">
<?=$view_comments;?>
</div>

<div class="modal fade" id="modal_image" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog  modal-lg" role="document">
    <div class="modal-content" style="height: 80%;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Slika</h4>
      </div>
      <div class="modal-body text-center" id="modal_image_content">
          <img src="" style="height:500px;">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Zapri</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

$('.close').click(function() {
    $('#alert_err').fadeOut(500);
    $('#alert_ok').fadeOut(500);
    $('#alert').fadeOut(500);
    $('alert').addClass('hidden'); 
});

$('.confirm').click(function() {
      return window.confirm("Izbriši temo?");
});

function open_modal(modal_id)
{
  event.stopPropagation();
  $(modal_id).modal('show');
}

function ajax_edit_subject_save()
{
  var form=$('#edit_subject');

  $.ajax({
        type: "POST",
        url: "<?=$this->url?>ajax_edit_subject_save",
        cache: false,               
        data:form.serialize(),
        dataType: "json",
        success: function(data){  
            if(data.status=='ok')
            {
                status('ok','Spremembe uspešno shranjene!');
            }
            else
            {
              var errors='';
              $.each(data.errfields, function(i, error){
                  errors+=error+'\n';
              });
              status('error',errors);
              
            }      
        },
        error: function(){                      
            alert('Prišlo je do napake pri nalaganju podatkov. Prosimo poskusite ponovno!');
        }
  });
}

function ajax_word_delete(id_word)
{

  $.ajax({
        type: "POST",
        url: "<?=$this->url?>ajax_word_delete",
        cache: false,               
        data:{
          id_word:id_word
        },
        dataType: "json",
        success: function(data){  
            if(data.status=='ok')
            { 
                //remove row
                $('#row-'+id_word).remove();
            }
            else
            {
              var errors='';
              $.each(data.errfields, function(i, error){
                  errors+=error+'\n';
              });
              status('error',errors);
              
            }      
        },
        error: function(){                      
            alert('Prišlo je do napake pri nalaganju podatkov. Prosimo poskusite ponovno!');
        }
  });
}

function status(status,text)
{
  $('#alert').fadeIn(400);
  //change div color
  if(status=='ok')
  {
    $('#alert').removeClass();
    $('#alert').addClass('alert alert-success');
    $('#alert_text').html("<span class='glyphicon glyphicon-ok text-success'></span> "+text);
  }
  else
  {
    $('#alert').removeClass();
    $('#alert').addClass('alert alert-danger');
    $('#alert_text').html("<span class='glyphicon glyphicon-exclamation-sign text-danger'></span> "+text);
  }
}

</script>