<?php

/*
Slovenski narečni atlas / Slovenian dialectal atlas
    Copyright (C) 2017  Gregor Šajn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
?>

<div class="container container-panel">
  <?
  $this->load->view('admin/index_view.php');
  ?>
  <div class="row" style="overflow: none">
    <?
    $this->load->view('admin/admin_nav.php');
    ?>
    
    <!-- tukaj se naloži poljuben pogled -->
    <div class="col-sm-9">
      <ol class="breadcrumb">
          <li><a href="<?=$this->url?>transcriptions">Fonetični zapisi</a></li>
          <li><a href="<?=$this->url?>edit_transcription/<?=$transcription['id']?>"><span class="ZRCola"><?=$transcription['phonetic_writing']?></span></a></li>
      </ol>
      <div class="row">
        <div class="col-sm-9"><h4 class="text-muted ZRCola"><?=$transcription['phonetic_writing']?></h4></div>
        <div class="col-sm-3 text-right">
        <a class="btn btn-danger confirm" href="<?=$this->url?>transcription_delete/<?=$transcription['id']?>" title="Odstrani transkripcijo">Odstrani</a></div>
      </div>
      <br>
      <!-- alerts -->
      <?
      if(isset($errfields) and $errfields)
      {
          $alert_text='<ul>';
          foreach($errfields as $errfield)
          {
            $alert_text.='<li>'.$errfield.'</li>';
          }
          $alert_text.='</ul>'
          ?>
          <div class="alert alert-danger fade in" id="alert_err" role="alert">
          <button type="button" class="close">
            <span aria-hidden="true">&times;</span>
          </button>
          <span class="glyphicon glyphicon-exclamation-sign text-danger"></span> <?=$alert_text?>         
          </div>
          <?
      }
      elseif(isset($status) and $status=='ok')
      {
        ?>
        <div class="alert alert-success fade in" id="alert_ok" role="alert">
        <button type="button" class="close">
            <span aria-hidden="true">&times;</span>
        </button>
        <span class="text-success glyphicon glyphicon-ok"></span> Spremembe uspešno shranjene!
        </div>
        <?
      }
      ?>

       <form class="form-horizontal form-border" action="<?=$this->url?>post_edit_transcription" method="POST" enctype="multipart/form-data" id="edit_subject">
        <input type="hidden" id="id" name="id" value="<?=$transcription['id']?>">
        <div class="form-group">
          <label for="phonetic_writing" class="col-sm-2 control-label">Fonetični zapis<span class="text-danger">*</span></label>
          <div class="col-sm-10">
            <input type="text" class="form-control input-md ZRCola" style="width:300px;" value="<?=$transcription['phonetic_writing']?>" id="phonetic_writing" name="phonetic_writing">
          </div>
        </div>
        
        <div class="form-group">
          <label for="subjects" class="col-sm-2 control-label">Tema<span class="text-danger">*</span></label>
          <div class="col-sm-10">
              <select class="form-control" style="width:300px;" id="subjects" name="subjects" onchange="ajax_update_words();">
              <? 
              foreach($subjects as $id=>$subject)
              {
                ?><option value="<?=$id?>" <?=$id==$transcription['id_subject']?'selected="selected"':''?> ><?=$subject?></option><?
              }
              ?>
              </select>
          </div>
        </div>
       <div class="form-group">
          <label for="words" class="col-sm-2 control-label">Beseda<span class="text-danger">*</span></label>
          <div class="col-sm-10">
              <select class="form-control" style="width:300px;" name="words" id="words" onchange="ajax_update_lexems();">
              <? 
              foreach($words as $id=>$word)
              {
                ?><option value="<?=$id?>" <?=$id==$transcription['id_word']?'selected="selected"':''?>><?=$word?></option><?
              }
              ?>
              </select>
          </div>
        </div>
         <div class="form-group">
            <label for="lexems_select" class="col-sm-2 control-label">Leksem<span class="text-danger">*</span></label>
            <div class="col-sm-10">
              <select class="form-control" id="lexems_select" name="lexems_select" style="width:300px;" >
              <? 
              foreach($lexems as $id=>$title)
              {
                ?><option value="<?=$id?>" <?=$id==$transcription['id_lexem']?'selected="selected"':''?>><?=$title?></option><?
              }
              ?>
              </select>  
              </div>
        </div>
        <div class="form-group">
          <label for="short_name" class="col-sm-2 control-label">Kraj<span class="text-danger">*</span></label>
          <div class="col-sm-10">
              <select class="form-control" style="width:300px;" name="locations" id="locations_select">
              <? 
              foreach($locations as $id=>$location)
              {
                ?><option value="<?=$id?>"  <?=$id==$transcription['id_location']?'selected="selected"':''?>><?=$location?></option><?
              }
              ?>
              </select>
          </div>
        </div>
        <div class="form-group">
        <label for="title" class="col-sm-2 control-label">Zvočni posnetek</label>
         <div class="col-sm-10">
            <?
            if($transcription['audio'])
            {
              ?>
              <div style="padding-top:2px;">
              <audio controls>
                  <source src="<?php echo base_url().$transcription['audio']?>" type="audio/mpeg">
              </audio>
                 <div>
                 <a href="javascript:;" onclick="if(confirm('Izbriši zvočni posnetek?')){ajax_audio_delete(<?=$transcription['id']?>,'<?=$transcription['audio']?>');}" class="text-danger"><span>Odstrani zvočni posnetek</span></a>
                 </div>
              </div>
              <?
            }
            else
            {
              ?>
              <input type="file" class="upload-btn" id="audio_file" name="audio_file">
              <?
            }
            ?>
            
          </div>
        </div>
        <div class="form-group">
          <label for="trans_text" class="col-sm-2 control-label">Beseda - besedilo</label>
          <div class="col-sm-10">
            <textarea type="text" class="form-control ZRCola textarea-size" style="width:300px;" id="trans_text" name="trans_text"><?=$transcription['trans_text']?></textarea>
          </div>
        </div>
        <br>
        <div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-success button-medium">Shrani</button>
            <button type="submit" class="btn btn-default button-medium" name=save_close>Shrani in zapri</button>
          </div>
        </div>
      </form>
      <br>
    </div>
  </div>
</div>


<script type="text/javascript">
$('.close').click(function() {
    $('#alert_err').fadeOut(500);
    $('#alert_ok').fadeOut(500);
});

$('.confirm').click(function() {
      return window.confirm("Izbriši transkripcijo?");
});


function ajax_update_words()
{
  var id_subject=$("#subjects option:selected").val();

  $.ajax({
        type: "POST",
        url: "<?=$this->url?>ajax_update_words",
        cache: false,               
        data: {
          id_subject: id_subject
        },
        dataType: "json",
        success: function(data){ 
          if(data.status=='ok')
          {
            var options;
            $('#words').empty();
           
            $.each(data.words, function(id, title){
                options += '<option value=' + id + '>' + title + '</option>';
            });
            
            $('#words').append(options);

            var locations;
            $('#locations_select').empty();
           
            //sort by name
            var sortable = [];
            for (var location in data.locations) {
                sortable.push([location, data.locations[location]]);
            }
            var first=sortable.shift();
            sortable.sort(function(a, b) {
                return a[1].localeCompare(b[1]);
            });
            sortable.unshift(first);

            $.each(sortable, function(id, title){
                locations += '<option value=' + title[0] + '>' + title[1] + '</option>';
            });
            
            $('#locations_select').append(locations);

          }    
        },
        error: function(){                      
            alert('Prišlo je do napake pri nalaganju podatkov. Prosimo poskusite ponovno!');
        }
  });
}


function ajax_update_lexems()
{
  var id_word=$("#words option:selected").val();
  var id_subject=$("#subjects option:selected").val();

  $.ajax({
        type: "POST",
        url: "<?=$this->url?>ajax_update_lexems",
        cache: false,               
        data: {
          id_word: id_word,
          id_subject:id_subject,

        },
        dataType: "json",
        success: function(data){ 
          if(data.status=='ok')
          {
            var options;
            $('#lexems_select').empty();
           
            //sort by name
            var sortable = [];
            for (var lexem in data.lexems) {
                sortable.push([lexem, data.lexems[lexem]]);
            }
            var first=sortable.shift();
            sortable.sort(function(a, b) {
                return a[1].localeCompare(b[1]);
            });
            sortable.unshift(first);
           
            $.each(sortable, function(id, title){
                options += '<option value=' + title[0] + '>' + title[1] + '</option>';
            });
            
            $('#lexems_select').append(options);


          }    
        },
        error: function(){                      
            alert('Prišlo je do napake pri nalaganju podatkov. Prosimo poskusite ponovno!');
        }
  });
}

function ajax_audio_delete(id_transcription,audio)
{
  $.ajax({
        type: "POST",
        url: "<?=$this->url?>ajax_audio_delete",
        cache: false,               
        data:{
          id_transcription:id_transcription,
          audio,audio
        },
        dataType: "json",
        success: function(data){  
            if(data.status=='ok')
            { 
                location.reload();
            }
            else
            {
              var errors='';
              $.each(data.errfields, function(i, error){
                  errors+=error+'\n';
              });
              alert(errors);
            }      
        },
        error: function(){                      
            alert('Prišlo je do napake pri nalaganju podatkov. Prosimo poskusite ponovno!');
        }
  });
}

</script>