<?php

/*
Slovenski narečni atlas / Slovenian dialectal atlas
    Copyright (C) 2017  Gregor Šajn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

?>

<div class="container container-panel">
  <?
  $this->load->view('admin/index_view.php');
  ?>
  <div class="row" style="overflow: none">
    <?
    $this->load->view('admin/admin_nav.php');
    ?>
    
    <!-- tukaj se naloži poljuben pogled -->
    <div class="col-sm-9">
      <ol class="breadcrumb">
        <li><a href="<?=$this->url?>edit_content">Teme</a></li>
        <li><a href="#">Nova tema</a></li>
      </ol>
      <div class="row">
        <div class="col-sm-9"><h4 class="text-muted">Nova tema</h4></div>
        <div class="col-sm-3 text-right"></div>
      </div>
      <br>
      <!-- alerts -->
      <?
      if(isset($errfields) and $errfields)
      {
          $alert_text='<ul>';
          foreach($errfields as $errfield)
          {
            $alert_text.='<li>'.$errfield.'</li>';
          }
          $alert_text.='</ul>'
          ?>
          <div class="alert alert-danger fade in" id="alert_err" role="alert">
          <button type="button" class="close">
            <span aria-hidden="true">&times;</span>
          </button>
          <span class="glyphicon glyphicon-exclamation-sign text-danger"></span> <?=$alert_text?>
          </div>
          <?
      }
      ?>
      <form class="form-horizontal form-border" action="<?=$this->url?>post_add_subject" method="POST" id="add_subject">
        <div class="form-group">
          <label for="title" class="col-sm-2 control-label">Naziv teme<span class="text-danger">*</span></label>
          <div class="col-sm-10">
            <input type="text" class="form-control input-md" value="<?=$subject['title']?>" id="title" name="title">
          </div>
        </div>
        
        <div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-success button-medium">Shrani</button>
            <button type="submit" class="btn btn-default button-medium" name=save_close>Shrani in zapri</button>
          </div>
        </div>
      </form>
      <br>
    </div>
  </div>
</div>


<script type="text/javascript">

$('.close').click(function() {
    $('#alert_err').fadeOut(500);
});

</script>