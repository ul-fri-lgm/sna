<?php

/*
Slovenski narečni atlas / Slovenian dialectal atlas
    Copyright (C) 2017  Gregor Šajn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

?>

<!-- errors -->
<div class="container container-panel">
  <div class="page-header">
    <div class="row">
      <div class="col-sm-9">
        <h3>Nadzorna plošča</h3>
      </div>
      <div class="col-sm-3 text-right">
        <div class="btn-group" style="margin-top:20px;">
          <a href="<?=base_url()?>" target="_blank" class="btn btn-default" title="Na prvo stran">Nazaj na karto</a>
        </div>
      </div>
    </div>
  </div>
  <?
      if(isset($errfields) and $errfields)
        {
            ?>
            <div class="alert alert-danger fade in center-half" id="alert_err" role="alert">
            <button type="button" class="close">
              <span aria-hidden="true">&times;</span>
            </button>
            <span class="glyphicon glyphicon-exclamation-sign text-danger"></span> <?=$errfields?>
            </div>
            <?
        }

  ?>



  <div class="panel panel-border center-half">
    <div class="panel-heading panel-border-heading">Prijava v nadzorno ploščo</div>
    <div class="panel-body" >
      <form method="post" action="<?=$uri?>">
        <div class="form-group">
          <label for="username">Uporabinško ime</label>
          <input type="text" class="form-control" id="username" name="username" autocomplete="off" placeholder="Uporabniško ime">
        </div>
        <div class="form-group">
          <label for="password">Geslo</label>
          <input type="password" class="form-control" id="password" name="password" autocomplete="off" placeholder="Geslo">
        </div>
        <button type="submit" class="btn btn-default">Prijava</button>
      </form>
    </div>
  </div>

</div>

<script type="text/javascript">
$(function() {
  $('.panel-border').hover(function() {
    $('.panel-border-heading').css('border-bottom', '2px solid #66afe9');
  }, function() {
    $('.panel-border-heading').css('border-bottom', '2px solid #bdbdbd');
  });
});

$('.close').click(function() {
    $('#alert_err').fadeOut(500);
});

</script>