<?php

/*
Slovenski narečni atlas / Slovenian dialectal atlas
    Copyright (C) 2017  Gregor Šajn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

?>

<div class="container container-first-page col-md-12"> <!--class="container"-->
	<div class="row row-borders">
		<div>
			<div class="map-view-margin">
				<h3 >Slovenski narečni atlas</h3>
			</div>
			<div class="form col-md-6">
				<div class="form-group">
					<label for="subjects" class="control-label col-sm-1">Tema</label>
					<div class="col-sm-8">
						<select class="form-control map-view-margin input-sm selects" id="subjects" onchange="ajax_change_subject();">
							<? 
							foreach($subjects as $id=>$subject)
							{
								?><option value="<?=$id?>"><?=$subject?></option><?
							}
							?>
						</select>
					</div>
				</div>
				<br/>
				<br/>
				<div class="form-group">
					<label for="words" class="ccontrol-label col-sm-1">Beseda</label>
					<div class="col-sm-8">
						<select class="form-control map-view-margin input-sm selects" id="words" onchange="ajax_add_locations();">
							<? 
							foreach($words as $id=>$word)
							{
								?><option value="<?=$id?>"><?=$word?></option><?
							}
							?>
						</select>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row col-md-12">
		<!-- info panel -->
		<div class="col-md-2 info-panel">
			<!-- title -->
			<div id="map_title_div" class="hidden info-panel-title">
				<strong>Izbrana beseda: </strong>
				<br/>
		    	<span id="map_title" class="map-title"></span> <span id="trans_count" class=""></span>
		    </div>

		    <!-- image -->
			<div id="image" class="hidden info-panel-image img-responsive">
			    <strong><span class="">Slika: </span></strong><br>
				<img id="word_image" class="info-panel-image-borders img-responsive"  src=""/> <br/>
			</div>

			<br/>
			<br/>
			<!-- comment button -->
			<div id="button_comment" class="hidden info-panel-comment">
				<strong><span>Komentar: </span></strong><br>
				<button type="button" class="btn btn-block btn-sm info-panel-btn" data-toggle="modal" data-target="#modal_comment">
				 	Odpri komentar h karti <span class="glyphicon glyphicon-book"></span>
				</button>
			</div>
		</div>
		<!-- actual map -->
		<div style="margin-bottom: 40px; margin-top: 15px" class="col-md-7">
			<div class="text-right">
			<?
			//load svg map
			$this->load->view('home/map_view.php');
			?>
			</div>
		</div>
        <!-- map legend -->
        <div class="col-md-3" id="legend" style="max-width: 270px; margin-top: 15px; margin-left: auto; margin-right: auto">
            <?
            $this->load->view('home/legend_view.php');
            ?>
        </div>
	</div>

	<div id="comment">
		<!-- Modal -->
		<div class="modal fade" id="modal_comment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		  <div class="modal-dialog  modal-lg" role="document">
		    <div class="modal-content" style="height: 80%;">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title" id="myModalLabel">Komentar h karti</h4>
		      </div>
		      <div class="modal-body" id="modal_comment_content">
		        <iframe id="modal_iframe" frameBorder="0" src=""></iframe>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn info-panel-btn" data-dismiss="modal">Zapri</button>
		      </div>
		    </div>
		  </div>
		</div>
	</div>
</div>
<div id="modals">
<?
	$this->load->view('home/transcription_popup_view.php');
?>	
</div>


<script type="text/javascript">

 function ajax_add_locations()
 {
 	var id_subject=$("#subjects option:selected").val();

 	var svgNS = "http://www.w3.org/2000/svg";
	var xlinkNS = "http://www.w3.org/1999/xlink";

 	var id_word=$("#words option:selected").val();

 	//console.log(id_word);

	$.ajax({
        type: "POST",
        url: "<?=$this->url?>ajax_add_locations",
        cache: false,               
        data: {
        	id_word: id_word,
        	id_subject:id_subject,
        },
        dataType: "json",
        success: function(data){
            //console.log(data);
        	if(data.status=='ok')
        	{
        	    //console.log(data);
        		//update modals
        		$('#modals').html(data.view_modals);

        		//update legend
        		$('#legend').html(data.view_legend);

        		//update image and commet
        		//map comment
        		if(data.word.comment)
        		{
        			$('#button_comment').removeClass('hidden');
        			$('#modal_iframe').attr('src',data.word.comment);

        		}
        		else
        		{
        			$('#button_comment').addClass('hidden');
        			$('#modal_iframe').attr('src','');
        		}

        		//map image
        		if(data.word.image)
        		{
        			$('#image').removeClass('hidden');
        			$('#word_image').attr('src',data.word.image);

        		}
        		else if(id_word!=0)
        		{
        			$('#image').addClass('hidden');
        			$('#word_image').attr('src','');
        		}
        		else
        		{
        			$('#image').addClass('hidden');
        			$('#word_image').attr('src','');
        		}


        		//map title
        		if(data.word.title)
        		{
        			$('#map_title_div').removeClass('hidden');
        			$('#map_title').text(data.word.title);
        		}
        		else
        		{
        			$('#map_title_div').addClass('hidden');
        			$('#map_title').text('');
        		}

        		//trans count
        		if(data.transcriptions_count)
        		{
        			$('#trans_count').removeClass();
        			$('#trans_count').addClass("text-success");
        			$('#trans_count').text("(Fonetični zapisi: "+data.transcriptions_count+")");
        		}
        		else
        		{
        			$('#trans_count').removeClass();
        			$('#trans_count').addClass('text-danger');
        			$('#trans_count').text("(Ni fonetičnih zapisov)");
        		}

        		//empty current markers
        		$("#markers").empty();

        		var svg = d3.select("body").select("#Layer_1");
        		svg.select('#pins').remove();
        		var defs = svg.append('defs').attr('id','pins');

        		//foreach and get pins for each transcription
        		$.each(data.words_transcriptions, function(i, transcription){
        			/* delujoča
        			var svg = d3.select("body").select("#Layer_1");
				    svg.append('defs').append("g").attr("id","pin"+i).append("image").attr("xlink:href",transcription.pin).attr("height","25px").attr("width","25px")
				    */


  
        			var g =  defs.append("g").attr("id","pin"+i);
        			var counter = 1;

        			if(transcription.id)
        			{
        				g.append("text").attr('id','text-'+transcription.pin1).attr("x","0px").attr("y","6px").attr("font-size",transcription.icon_size1 + 'px').attr('class','SIMBola').style("fill", transcription.icon_color1).text(transcription.pin1);
                        if(transcription.pin2){
                            counter++;
                            let dx2 = Number(transcription.icon_size1);
                            g.append("text").attr('id','text-'+transcription.pin2).attr("x",'+'+dx2+'px').attr("y","6px").attr("font-size",transcription.icon_size2 + 'px').attr('class','SIMBola').style("fill", transcription.icon_color2).text(transcription.pin2);
                        }
                        if(transcription.pin3){
                            counter++;
                            let dx3 = Number(transcription.icon_size1) + Number(transcription.icon_size2);
                            g.append("text").attr('id','text-'+transcription.pin3).attr("x",'+'+dx3+'px').attr("y","6px").attr("font-size",transcription.icon_size3 + 'px').attr('class','SIMBola').style("fill", transcription.icon_color3).text(transcription.pin3);
                        }
                        if(transcription.pin4){
                            counter++;
                            let dx4 = Number(transcription.icon_size1) + Number(transcription.icon_size2) + Number(transcription.icon_size3);
                            g.append("text").attr('id','text-'+transcription.pin4).attr("x",'+'+dx4+'px').attr("y","6px").attr("font-size",transcription.icon_size4 + 'px').attr('class','SIMBola').style("fill", transcription.icon_color4).text(transcription.pin4);
                        }
        			}
        			else
        			{
        				g.append("text").attr('id','text-'+transcription.pin1).attr("x","-5px").attr("y","6px").attr("font-size","18pt").text("/");
        			}

                    let element;
        			let dx;
                    if(counter === 1){
                        dx = Number(transcription.icon_size1);
                    }
                    if(counter === 2){
                        dx = Number(transcription.icon_size1) + Number(transcription.icon_size2);
                    }
                    if(counter === 3){
                        dx = Number(transcription.icon_size1) + Number(transcription.icon_size2) + Number(transcription.icon_size3);
                    }
                    if(counter === 4){
                        dx = Number(transcription.icon_size1) + Number(transcription.icon_size2) + Number(transcription.icon_size3) + Number(transcription.icon_size4);
                    }

                    let name;
                    //console.log('star: ', transcription.star);
                    if(transcription.star === '1'){
                        name = transcription.short_name + '*';
                    }
                    else{
                        name = transcription.short_name;
                    }

				    g.append("text").attr("x",dx+"px").attr("y","4px").attr("font-size","12pt").text(name);


				    // Create an SVG <use> element
				    var use = document.createElementNS(svgNS, "use");

				    use.setAttributeNS(xlinkNS, "href", "#pin"+i);
				    use.setAttribute("x", transcription.x);
				    use.setAttribute("y", transcription.y);
				    use.setAttribute("class", "icons_all icons-"+transcription.code_title);

				    if(transcription.id)
				    {
				    	use.setAttribute("data-toggle","modal");
				    	use.setAttribute("data-target","#my_modal-"+i);
				    }
				    else
				    {
				    	use.style.cursor="default";
				    }
				   
				    document.getElementById("markers").appendChild(use);
				});
        	}    
        },
        error: function(e){
            //console.log(e);
            alert('Prišlo je do napake pri nalaganju podatkov. Prosimo poskusite ponovno!');
        }
 	});
 }

function ajax_change_subject()
{
	var id_subject=$("#subjects option:selected").val();

	$.ajax({
        type: "POST",
        url: "<?=$this->url?>ajax_change_subject",
        cache: false,               
        data: {
        	id_subject: id_subject
        },
        dataType: "json",
        success: function(data){ 
        	if(data.status=='ok')
        	{
        		var options;
        		$('#words').empty();

    			$.each(data.words, function(id, title){
			    	options += '<option value=' + id + '>' + title + '</option>';
				});
				
				$('#words').append(options);

				//update map - za praznjenje ko ni izbrana nobena tema
				ajax_add_locations();
        	}    
        },
        error: function(){                      
            alert('Prišlo je do napake pri nalaganju podatkov. Prosimo poskusite ponovno!');
        }
 	});
}
</script>

