<?php

/*
Slovenski narečni atlas / Slovenian dialectal atlas
    Copyright (C) 2017  Gregor Šajn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/


if(isset($legend) and $legend)
{
	$last = array_pop($legend);
	ksort($legend);
	array_push($legend,$last);
	?>
	<div class="info-panel-image-borders legend">
		<div class="legend_title text-left" style="font-size:10px">
			<strong><span>Legenda</span></strong><br>
			<span>* sklic na komentar</span>
		</div>
		<table class="table" style="font-size:10px">
		  <thead>
		      <tr>
		      	<th class="text-center"><input style="width:10px;height:10px;" type="checkbox" class="toggle_all" checked="1"/></th>
		      	<th class="text-center">Znak</th>
			    <th class="text-center">Leksem</th>
		      </tr> 
		  </thead>
		  <tbody>

		   <?
		  	foreach($legend as $title=>$row)
		  	{
		  		?>
			   	<tr>
			   		<td class="text-center"><input style="width:10px;height:10px;" type="checkbox" checked="1" id="icons-<?=$row['code_title']?>" class="toggle_icons"/></td>
			      	<td class="text-center SIMBola"><span style="color:<?=$row['icon_color1']?>; font-size: <?= $row['icon_size1'] && $row['icon_size1'] === 'normal' ? '17px' : '13px'?>"><?=$row['icon1']?></span><span style="color:<?=$row['icon_color2']?>; font-size: <?= $row['icon_size2'] && $row['icon_size2'] === 'normal' ? '17px' : '13px'?>"><?=$row['icon2'] && $row['icon2']!== 0 ? $row['icon2'] : ''?></span><span style="color:<?=$row['icon_color3']?>; font-size: <?= $row['icon_size3'] && $row['icon_size3'] === 'normal' ? '17px' : '13px'?>"><?=$row['icon3'] && $row['icon3']!== 0 ? $row['icon3'] : ''?></span><span style="color:<?=$row['icon_color4']?>; font-size: <?= $row['icon_size4'] && $row['icon_size4'] === 'normal' ? '17px' : '13px'?>"><?=$row['icon4'] && $row['icon4']!== 0 ? $row['icon4'] : ''?></span></td>
			      	<td class="text-center">
			      	<?
			      	if($title=='no_title')
			      	{
			      		echo "ni odgovora";
			      	}
			      	else
			      	{
			      		echo $title;
			      	}
			      	?>
			      	</td>
			     </tr>
			     <?
		  	}
			
		  ?>
		  	
		  </tbody>
		</table>
	</div>
	<?
}
?>

<script type="text/javascript">
$('.toggle_icons').click(function() {
	var icons = $(this).attr('id');

	$('.'+icons).toggle();
});


 $(".toggle_all").click(function () {
 	if($(this).is(":checked"))   
        $(".icons_all").show();
    else
        $(".icons_all").hide();

    $('input:checkbox').not(this).prop('checked', this.checked);
 });


</script>
