<?php

/*
Slovenski narečni atlas / Slovenian dialectal atlas
    Copyright (C) 2017  Gregor Šajn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/


if(isset($words_transcriptions) and $words_transcriptions)
{
  foreach($words_transcriptions as $id=>$transcription)
  {
    if(isset($transcription['phonetic_writing']))
    {
      ?>
      <div class="modal fade" id="my_modal-<?=$id?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
     <div class="modal-div-vertical-center">
        <div class="modal-dialog modal-md modal-vertical-center " role="document">
          <div class="modal-content" id="modal-content">
              <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <span class="modal-title text14 bold" id="title"><strong><?=$transcription['name']?></strong>:</span><span class="text12"> <?=$transcription['dialect']?>, <?=$transcription['subdialect']?><?=(isset($transcription['subsubdialect']) and $transcription['subdialect'])?', '.$transcription['subsubdialect']:''?></span>
          </div>
          <!--<span class="ZRCola"><?=$transcription['phonetic_writing']?></span>-->
          <div class="modal-body" id="content">
            <div class="row">
                <div class="col-md-4 bold modal-div-content"><span class="text14 bold"><?=$transcription['title']?></span></div>
                <div class="col-md-8 ZRCola modal-div-content text12">
                 <span class="ZRCola text14"><?=$transcription['phonetic_writing']?></span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 bold modal-div-content"><span class="text12 bold">Beseda - besedilo</span></div>
                <div class="col-md-8 ZRCola modal-div-content text12">
                 <span class="text12"><?=$transcription['trans_text']?></span>
                </div>
            </div><br>
            <div class="row">
              <div class="col-md-4 bold modal-div-content"><span class="text12 bold">Zvočni posnetek</span></div>
              <div class="col-md-8">
                <audio controls>
                  <source src="<?php echo base_url().$transcription['audio']?>" type="audio/mpeg">
                </audio>
              </div>
            </div>
          </div>

          <div class="modal-footer">
            <button type="button" class="btn info-panel-btn" data-dismiss="modal">Zapri</button>
          </div>
          </div>
        </div>
      </div>
    </div>
    <?

    }
    ?>
    
    <?
  }
}
?>
