<?php

/*
Slovenski narečni atlas / Slovenian dialectal atlas
    Copyright (C) 2017  Gregor Šajn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

?>
<div class="container container-panel">
  <div class="page-header">
    <div class="row">
      <div class="col-sm-9">
        <h3>O spletni aplikaciji</h3>
      </div>
      <div class="col-sm-3 text-right">
        <div class="btn-group" style="margin-top:20px;">
          <a href="<?=base_url()?>" class="btn btn-default" title="Na prvo stran">Nazaj na karto</a>
        </div>
      </div>
    </div>
  </div>
  <br>
  <div>
    <h4 class="text-muted">Avtor</h4>
    <p>Avtor spletne aplikacije je Gregor Šajn, študent Fakultete za računalništvo in informatiko Univerze v Ljubljani. Aplikacijo je izdelal v okviru diplomske naloge v letu 2017 pod mentorstvom viš. pred. dr. Alenke Kavčič (UL FRI). <br>
	Izdelano spletno aplikacijo je z dodatnimi funkcionalnostmi nadgradil Nermin Jukan, študent Fakultete za računalništvo in informatiko Univerze v Ljubljani, v okviru predmeta Računalništvo v praksi II pod mentorstvom viš. pred. dr. Alenke Kavčič (avgust 2018).</p>
    <br>

    <h4 class="text-muted">Aplikacija</h4>

    <p>Slovenski narečni atlas / Slovenian dialectal atlas<br>
    Copyright &copy; 2017 Gregor Šajn (greg.sajn@gmail.com)<br><br>

    Ta program spada med prosto programje; lahko ga razširjate in/ali spreminjate pod pogoji Splošnega dovoljenja GNU (GNU General Public License), različice 3, kot ga je objavila ustanova Free Software Foundation.<br>

    Ta program se razširja v upanju, da bo uporaben, vendar BREZ VSAKRŠNEGA JAMSTVA; tudi brez posredne zagotovitve CENOVNE VREDNOSTI ali PRIMERNOSTI ZA DOLOČEN NAMEN. Za podrobnosti glejte besedilo GNU General Public License.<br>

    Skupaj s tem programom bi morali prejeti izvod Splošnega dovoljenja GNU (GNU General Public License). Podrobnosti licence so dostopne tudi na spletni strani <a href="http://www.gnu.org/licenses" target="_blank">http://www.gnu.org/licenses</a>.
	<br><br>
	Izvorna koda aplikacije je dosegljiva v repozitoriju Bitbucket: <a href="https://bitbucket.org/ul-fri-lgm/sna" target="_blank">https://bitbucket.org/ul-fri-lgm/sna</a>.
    </p>
    <br>
    <h4 class="text-muted">Pisavi ZRCola in SIMBola</h4>
    <p>
	Besedilo je bilo pripravljeno z vnašalnim sistemom ZRCola (<a href="http://zrcola.zrc-sazu.si" target="_blank">http://zrcola.zrc-sazu.si</a>), ki ga je na Znanstvenoraziskovalnem centru SAZU v Ljubljani (<a href="http://www.zrc-sazu.si" target="_blank">http://www.zrc-sazu.si</a>) razvil Peter Weiss. Peter Weiss je za potrebe SLA pripravil tudi nabor simbolov za kartiranje, in sicer je leta 2005 zasnoval pisavo 05 SIMBola na podlagi simbolov, ki so v rabi pri OLA, ALE in v drugih (predvsem slovanskih) jezikovnih atlasih. ZRC SAZU se za posredovano pisavo 05 SIMBola zahvaljujemo.
	</p>
	<br>
	<h4 class="text-muted">Karta slovenskih narečij</h4>
    <p>
	Slovenski narečni atlas (SNA) temelji na <i>Karti slovenskih narečij</i>. Karto sta priredila Tine Logar in Jakob Rigler (1983) na osnovi <i>Dialektološke karte slovenskega jezika</i> Frana Ramovša (1931), novejših raziskav in gradiva Inštituta za slovenski jezik ZRC SAZU, jo dopolnili Vera Smole in Jožica Škofic (2011) in nato še sodelavci Dialektološke sekcije ISJFR ZRC SAZU (2016).<br><br>
	&copy; Inštitut za slovenski jezik Frana Ramovša ZRC SAZU, Geografski inštitut Antona Melika ZRC SAZU ter Inštitut za antropološke in prostorske študije ZRC SAZU, 2016
	</p>
	<br>
	<h4 class="text-muted">Vsebina (narečne besede)</h4>
    <p>
	Vsebino Slovenskega narečnega atlasa (narečne besede, zvočni posnetki, analize) za temo Stare kmečke hiše so zbrali in pripravili študenti Filozofske fakultete Univerze v Ljubljani pod vodstvom prof. dr. Vere Smole in asist. dr. Mojce Kumin Horvat.
	<br>Vsem študentom, preteklim in bodočim, in njihovim informatorjem se zahvaljujemo za sodelovanje in njihov prispevek.
	</p>
    <br>
    <br>
    <span class="text-muted small"><strong>Zadnja sprememba:</strong> avgust 2018, Nermin Jukan</span>
  </div>
  <hr>
</div>
