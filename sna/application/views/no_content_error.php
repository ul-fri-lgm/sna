<?php

/*
Slovenski narečni atlas / Slovenian dialectal atlas
    Copyright (C) 2017  Gregor Šajn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

?>

<div class="container container-panel">
  <?
  $this->load->view('admin/index_view.php');
  ?>
  <div class="row" style="overflow: none">
    <?
    $this->load->view('admin/admin_nav.php');
    ?>
    <div class="col-sm-9">
        <div class="alert alert-danger fade in hidden" id="alert" role="alert">
	        <span id="alert_text"><span class="glyphicon glyphicon-exclamation-sign"></span> <?=$text?> <a href="<?=$link_back?>">Nazaj</a></span>
	     </div>
    </div>
  </div>
</div>



<script type="text/javascript">
$(document).ready(function() {
	$('#alert').fadeIn(400);
	$('#alert').removeClass('hidden');
    //$('alert').addClass('hidden');
});

</script>