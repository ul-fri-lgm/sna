Projekt SNA (Slovenski narečni atlas / Slovenian dialectal atlas)

Interaktivni atlas slovenskih narečnih besed, diploma Gregor Šajn, UL FRI, 2017
Dopolnitve aplikacije: Nermin Jukan, UL FRI, 2018

Direktorij projekta vsebuje:
- to datoteko readme.txt
- kodo aplikacije v direktoriju sna
- začetno podatkovno bazo v datoteki digital_atlas.sql
- navodila za namestitev aplikacije
- navodila za uporabo aplikacije
