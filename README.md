#O spletni aplikaciji

##Avtor
Avtor spletne aplikacije je Gregor Šajn, študent Fakultete za računalništvo in informatiko Univerze v Ljubljani. Aplikacijo je izdelal v okviru diplomske naloge v letu 2017 pod mentorstvom viš. pred. dr. Alenke Kavčič (UL FRI).
Izdelano spletno aplikacijo je z dodatnimi funkcionalnostmi nadgradil Nermin Jukan, študent Fakultete za računalništvo in informatiko Univerze v Ljubljani, v okviru predmeta Računalništvo v praksi II pod mentorstvom viš. pred. dr. Alenke Kavčič (avgust 2018).

##Aplikacija
Slovenski narečni atlas / Slovenian dialectal atlas
Copyright © 2017 Gregor Šajn (greg.sajn@gmail.com)

Ta program spada med prosto programje; lahko ga razširjate in/ali spreminjate pod pogoji Splošnega dovoljenja GNU (GNU General Public License), različice 3, kot ga je objavila ustanova Free Software Foundation.
Ta program se razširja v upanju, da bo uporaben, vendar BREZ VSAKRŠNEGA JAMSTVA; tudi brez posredne zagotovitve CENOVNE VREDNOSTI ali PRIMERNOSTI ZA DOLOČEN NAMEN. Za podrobnosti glejte besedilo GNU General Public License.
Skupaj s tem programom bi morali prejeti izvod Splošnega dovoljenja GNU (GNU General Public License). Podrobnosti licence so dostopne tudi na spletni strani http://www.gnu.org/licenses. 

Izvorna koda aplikacije je dosegljiva v repozitoriju Bitbucket: https://bitbucket.org/ul-fri-lgm/sna.

##Pisavi ZRCola in SIMBola
Besedilo je bilo pripravljeno z vnašalnim sistemom ZRCola (http://zrcola.zrc-sazu.si), ki ga je na Znanstvenoraziskovalnem centru SAZU v Ljubljani (http://www.zrc-sazu.si) razvil Peter Weiss. Peter Weiss je za potrebe SLA pripravil tudi nabor simbolov za kartiranje, in sicer je leta 2005 zasnoval pisavo 05 SIMBola na podlagi simbolov, ki so v rabi pri OLA, ALE in v drugih (predvsem slovanskih) jezikovnih atlasih. ZRC SAZU se za posredovano pisavo 05 SIMBola zahvaljujemo.

##Karta slovenskih narečij 
Slovenski narečni atlas (SNA) temelji na Karti slovenskih narečij. Karto sta priredila Tine Logar in Jakob Rigler (1983) na osnovi Dialektološke karte slovenskega jezika Frana Ramovša (1931), novejših raziskav in gradiva Inštituta za slovenski jezik ZRC SAZU, jo dopolnili Vera Smole in Jožica Škofic (2011) in nato še sodelavci Dialektološke sekcije ISJFR ZRC SAZU (2016).

© Inštitut za slovenski jezik Frana Ramovša ZRC SAZU, Geografski inštitut Antona Melika ZRC SAZU ter Inštitut za antropološke in prostorske študije ZRC SAZU, 2016

##Vsebina (narečne besede)
Vsebino Slovenskega narečnega atlasa (narečne besede, zvočni posnetki, analize) za temo Stare kmečke hiše so zbrali in pripravili študenti Filozofske fakultete Univerze v Ljubljani pod vodstvom prof. dr. Vere Smole in asist. dr. Mojce Kumin Horvat. 
Vsem študentom, preteklim in bodočim, in njihovim informatorjem se zahvaljujemo za sodelovanje in njihov prispevek.


Zadnja sprememba: avgust 2018, Nermin Jukan
